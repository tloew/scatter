/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <scatter/TextOptions.hpp>

namespace scatter
{
    TextOptions::TextOptions()
    {
        size_ = 20;
        rotation_ = 0.0;
        colour_ = Colour::black;
        anchor_ = Anchor::CENTER;
        bold_ = false;
    }

    TextOptions::~TextOptions() {}

    TextOptions &TextOptions::setSize(const int &size)
    {
        size_ = size;

        return *this;
    }

    TextOptions &TextOptions::setRotation(const double &rotation)
    {
        rotation_ = rotation;

        return *this;
    }

    TextOptions &TextOptions::setColour(const Colour &colour)
    {
        colour_ = colour;

        return *this;
    }

    TextOptions &TextOptions::setAnchor(const Anchor &anchor)
    {
        anchor_ = anchor;

        return *this;
    }

    TextOptions &TextOptions::setBold(const bool &bold)
    {
        bold_ = bold;

        return *this;
    }

    const int &TextOptions::getSize() const
    {
        return size_;
    }

    const double &TextOptions::getRotation() const
    {
        return rotation_;
    }

    const Colour &TextOptions::getColour() const
    {
        return colour_;
    }

    const Anchor &TextOptions::getAnchor() const
    {
        return anchor_;
    }

    const bool &TextOptions::getBold() const
    {
        return bold_;
    }

}  // namespace scatter