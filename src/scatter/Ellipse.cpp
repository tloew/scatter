/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <scatter/Ellipse.hpp>

namespace scatter
{
    Ellipse::Ellipse(const Point &center, const double &minor, const double &major, const double &rotation)
      : center_(center), minor_(minor), major_(major), rotation_(rotation)
    {}

    Ellipse::~Ellipse() {}

    const Point &Ellipse::getCenter() const
    {
        return center_;
    }

    const double &Ellipse::getMinor() const
    {
        return minor_;
    }

    const double &Ellipse::getMajor() const
    {
        return major_;
    }

    const double &Ellipse::getRotation() const
    {
        return rotation_;
    }

}  // namespace scatter