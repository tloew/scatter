/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <memory>
#include <scatter/AxisOptions.hpp>
#include <scatter/ColourScheme.hpp>
#include <scatter/LegendOptions.hpp>
#include <scatter/TextOptions.hpp>

namespace scatter
{
    /**
     * @class PlotOptions Options.hpp <scatter/Options.hpp>
     * @brief [brief description]
     * @details [long description]
     */
    class PlotOptions
    {
      public:
        /**
         * @brief [brief description]
         * @details [long description]
         */
        PlotOptions();

        /**
         * @brief [brief description]
         * @details [long description]
         */
        virtual ~PlotOptions();

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        AxisOptions &getAxisOptions();

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        LegendOptions &getLegendOptions();

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        TextOptions &getTitleOptions();

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        TextOptions &getTextOptions();

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        const AxisOptions &getAxisOptions() const;

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        const LegendOptions &getLegendOptions() const;

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        void setTitle(const std::string &title_text);

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        const std::string &getTitle() const;

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        const TextOptions &getTitleOptions() const;

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        const TextOptions &getTextOptions() const;

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        const ColourScheme &getColourScheme() const;

      private:
        ///
        AxisOptions axis_;

        ///
        LegendOptions legend_;

        ///
        TextOptions title_;

        ///
        std::string title_text_;

        ///
        TextOptions text_;

        ///
        std::shared_ptr<ColourScheme> colour_scheme_;
    };

}  // namespace scatter