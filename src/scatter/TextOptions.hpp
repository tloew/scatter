/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <scatter/Options.hpp>

namespace scatter
{
    /**
     * @class TextOptions Options.hpp <scatter/Options.hpp>
     * @brief [brief description]
     * @details [long description]
     */
    class TextOptions
    {
      public:
        /**
         * @brief [brief description]
         * @details [long description]
         */
        TextOptions();

        /**
         * @brief [brief description]
         * @details [long description]
         */
        ~TextOptions();

        /**
         * @brief [brief description]
         * @details [long description]
         */
        TextOptions &setSize(const int &size);

        /**
         * @brief [brief description]
         * @details [long description]
         */
        TextOptions &setRotation(const double &rotation);

        /**
         * @brief [brief description]
         * @details [long description]
         */
        TextOptions &setColour(const Colour &colour);

        /**
         * @brief [brief description]
         * @details [long description]
         */
        TextOptions &setAnchor(const Anchor &anchor);

        /**
         * @brief [brief description]
         * @details [long description]
         */
        TextOptions &setBold(const bool &bold);

        /**
         * @brief [brief description]
         * @details [long description]
         */
        const int &getSize() const;

        /**
         * @brief [brief description]
         * @details [long description]
         */
        const double &getRotation() const;

        /**
         * @brief [brief description]
         * @details [long description]
         */
        const Colour &getColour() const;

        /**
         * @brief [brief description]
         * @details [long description]
         */
        const Anchor &getAnchor() const;

        /**
         * @brief [brief description]
         * @details [long description]
         */
        const bool &getBold() const;

      private:
        /// text size
        int size_;
        /// angle of the text line to the positive x-axis (in rad)
        double rotation_;
        /// text Colour
        Colour colour_;
        /// how to anchor the text at the given coordinate
        Anchor anchor_;
        /// true for bold
        bool bold_;
    };
}  // namespace scatter