/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <cmath>
#include <iostream>
#include <scatter/Axis.hpp>
#include <scatter/Legend.hpp>
#include <scatter/Plot.hpp>
#include <scatter/Point.hpp>
#include <scatter/plots/ArrowPlot.hpp>
#include <scatter/plots/BarPlot.hpp>
#include <scatter/plots/BoxPlot.hpp>
#include <scatter/plots/EllipsePlot.hpp>
#include <scatter/plots/FunctionPlot.hpp>
#include <scatter/plots/HistogramPlot.hpp>
#include <scatter/plots/LinePlot.hpp>
#include <scatter/plots/PlotBase.hpp>
#include <scatter/plots/ScatterPlot.hpp>
#include <scatter/render/Renderer.hpp>
#include <scatter/render/Transform.hpp>

namespace scatter
{
    Plot::Plot(const std::string &title, const PlotOptions &options) : options_(options)
    {
        axis_ = std::make_unique<Axis>(&options_.getAxisOptions(), &options_.getTextOptions());
        legend_ = std::make_unique<Legend>(&options_.getLegendOptions(), &options_.getTextOptions());
        options_.setTitle(title);
    }

    Plot::~Plot() {}

    Plot::Ptr Plot::create(const PlotOptions &options)
    {
        return create("", options);
    }

    Plot::Ptr Plot::create(const std::string &title, const PlotOptions &options)
    {
        return std::shared_ptr<Plot>(new Plot(title, options));
    }

    bool Plot::save(const std::string &file, const FigureOptions &options)
    {
        // create a temporary figure
        Figure figure(options);
        figure.add(shared_from_this());
        figure.save(file);

        return true;
    }

    void Plot::render(Renderer &renderer, const Point &origin, const double &width, const double &height)
    {
        if (options().getAxisOptions().getYmin() == options().getAxisOptions().getYmax())
        {
            double ymax = options().getAxisOptions().getYmax();

            for (unsigned i = 0; i < plots_.size(); ++i)
            {
                if (plots_[i]->getYmax() > ymax)
                {
                    ymax = plots_[i]->getYmax();
                }
            }

            ymax = std::ceil(ymax / options().getAxisOptions().getYticks()) * options().getAxisOptions().getYticks();

            options().getAxisOptions().setYmax(ymax);
        }

        Transform transform(origin, width, height, options_);

        renderer.restrictArea(transform.absolute(0.0, 1.0), transform.absolute(1.0, 0.0));

        renderer.text(transform.absolute(0.5, 1.0), options_.getTitle(), options_.getTitleOptions());

        axis_->render(renderer, transform);

        renderer.restrictArea(transform.relative(0.0, 1.0), transform.relative(1.0, 0.0));

        for (unsigned i = 0; i < plots_.size(); ++i)
        {
            if (plots_[i]->getColour() == Colour())
            {
                plots_[i]->setColour(options().getColourScheme().getColour(i));
            }

            plots_[i]->render(renderer, transform);

            if (!plots_[i]->getLegendEntry().label.empty())
            {
                legend_->add(plots_[i]->getLegendEntry());
            }
        }

        legend_->render(renderer, transform);
    }

    PlotOptions &Plot::options()
    {
        return options_;
    }

}  // namespace scatter