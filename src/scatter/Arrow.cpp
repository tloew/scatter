/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <cmath>
#include <scatter/Arrow.hpp>

namespace scatter
{
    Arrow::Arrow(const Point &p1, const Point &p2) : points_({ Point(0.0, 0.0), Point(0.0, 0.0), Point(0.0, 0.0), Point(0.0, 0.0) })
    {
        points_[0] = p1;
        points_[1] = p2;

        double dx = p2.x() - p1.x();
        double dy = p2.y() - p1.y();

        double length = std::sqrt(std::pow(dx, 2) + std::pow(dy, 2)) / 4.0;
        double angle = std::atan2(dy, dx);

        points_[2] = p2 + length * Point(std::cos(angle - 5.0 * M_PI / 6.0), std::sin(angle - 5.0 * M_PI / 6.0));
        points_[3] = p2 + length * Point(std::cos(angle + 5.0 * M_PI / 6.0), std::sin(angle + 5.0 * M_PI / 6.0));
    }

    Arrow::Arrow(const Point &p1, const double &length, const double &angle)
      : points_({ Point(0.0, 0.0), Point(0.0, 0.0), Point(0.0, 0.0), Point(0.0, 0.0) })
    {
        points_[0] = p1;
        points_[1] = p1 + length * Point(std::cos(angle), std::sin(angle));

        points_[2] = points_[1] + length / 3.0 * Point(std::cos(angle - 5.0 * M_PI / 6.0), std::sin(angle - 5.0 * M_PI / 6.0));
        points_[3] = points_[1] + length / 3.0 * Point(std::cos(angle + 5.0 * M_PI / 6.0), std::sin(angle + 5.0 * M_PI / 6.0));
    }

    Arrow::~Arrow() {}

    const std::array<Point, 4> &Arrow::getPoints() const
    {
        return points_;
    }

}  // namespace scatter