/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <array>
#include <scatter/Point.hpp>

namespace scatter
{
    /**
     * @brief [brief description]
     * @details [long description]
     */
    class Arrow
    {
      public:
        /**
         * @brief constructor from two Point
         * @details [long description]
         *
         * @param p1 Point 1 (tail)
         * @param p2 Point 2 (head)
         */
        Arrow(const Point &p1, const Point &p2);

        /**
         * @brief constructor from start Point, angle and length
         * @details [long description]
         *
         * @param p1 start Point
         * @param length length of the Arrow
         * @param angle angle of Arrow to the horizontal axis (in rad)
         */
        Arrow(const Point &p1, const double &length, const double &angle);

        /**
         * @brief destructor
         * @details [long description]
         */
        virtual ~Arrow();

        /**
         * @brief access the Point array representing the Arrow
         * @details this include the Arrow head
         * @return Point array
         */
        const std::array<Point, 4> &getPoints() const;

      protected:
      private:
        /// Point array representing the Arrow
        std::array<Point, 4> points_;
    };

}  // namespace scatter