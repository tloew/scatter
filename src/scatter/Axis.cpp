/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <cmath>
#include <iomanip>
#include <scatter/Axis.hpp>
#include <scatter/render/Renderer.hpp>
#include <scatter/render/Transform.hpp>
#include <sstream>

namespace scatter
{
    Axis::Axis(const AxisOptions *const options, const TextOptions *const text_options) : options_(options), text_options_(text_options) {}

    Axis::~Axis() {}

    void Axis::render(Renderer &renderer, const Transform &transform)
    {
        renderer.line(transform(options_->getXmin(), options_->getYmax()), transform(options_->getXmax(), options_->getYmax()), 3.0);
        renderer.line(transform(options_->getXmin(), options_->getYmin()), transform(options_->getXmin(), options_->getYmax()), 3.0);
        renderer.line(transform(options_->getXmin(), options_->getYmin()), transform(options_->getXmax(), options_->getYmin()), 3.0);
        renderer.line(transform(options_->getXmax(), options_->getYmin()), transform(options_->getXmax(), options_->getYmax()), 3.0);

        if (!options_->getShow())
        {
            return;
        }

        TextOptions text_options = *text_options_;

        if (options_->getShowXticks() && options_->getXticks() > 0)
        {
            double xdiff = options_->getXmax() - options_->getXmin();
            double dx = xdiff / options_->getXticks();

            for (int i = 0; i <= options_->getXticks(); ++i)
            {
                Point p = transform(options_->getXmin() + i * dx, options_->getYmin());

                renderer.line(p, p + Point(0.0, 5.0), Colour(), 3.0);

                std::stringstream label;
                label << std::setprecision(options_->getXPrecision()) << options_->getXmin() + i * dx;

                text_options.setAnchor(Anchor::NORTH);
                renderer.text(p + Point(0.0, 5.0), label.str(), text_options);

                if (options_->getShowGrid())
                {
                    Point p2 = transform(options_->getXmin() + i * dx, options_->getYmax());

                    renderer.line(p, p2, Colour(0.5, 0.5, 0.5, 0.5), 3.0);
                }
            }
        }

        if (options_->getShowYticks() && options_->getYticks() > 0)
        {
            double ydiff = options_->getYmax() - options_->getYmin();
            double dy = ydiff / options_->getYticks();

            double y_min = options_->getYmin();
            // options_->getYmin() == 0.0 ? options_->getYmin() :
            //                                             options_->getYticks() * std::floor(std::abs(options_->getYmin() / options_->getYticks())) *
            //                                               options_->getYmin() / std::abs(options_->getYmin());

            for (int i = 0; i <= options_->getYticks(); ++i)
            {
                Point p = transform(options_->getXmin(), y_min + i * dy);

                renderer.line(p - Point(5.0, 0.0), p, Colour(), 3.0);

                std::stringstream label;
                label << std::setprecision(options_->getYPrecision()) << y_min + i * dy;

                text_options.setAnchor(Anchor::EAST);
                renderer.text(p - Point(5.0, 0.0), label.str(), text_options);

                if (options_->getShowGrid())
                {
                    Point p2 = transform(options_->getXmax(), y_min + i * dy);

                    renderer.line(p, p2, Colour(0.5, 0.5, 0.5, 0.5), 3.0);
                }
            }
        }

        text_options.setAnchor(Anchor::SOUTH);
        renderer.text(transform.absolute(0.5, 0.0), options_->getXlabel(), text_options);

        text_options.setRotation(90);
        text_options.setAnchor(Anchor::NORTH);
        renderer.text(transform.absolute(0.0, 0.5), options_->getYlabel(), text_options);
    }

}  // namespace scatter