/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <scatter/Colour.hpp>
#include <string>

namespace scatter
{
    enum class Marker
    {
        CIRCLE,
        ASTERISK,
        CROSS,
        PLUS,
        TRIANGLE,
        DIAMOND,
        SQUARE,
        STAR
    };

    enum class Anchor
    {
        CENTER,
        NORTH,
        NORTH_EAST,
        EAST,
        SOUTH_EAST,
        SOUTH,
        SOUTH_WEST,
        WEST,
        NORTH_WEST
    };

    /**
     * @class OptionsBase Options.hpp <scatter/Options.hpp>
     * @brief [brief description]
     * @details [long description]
     */
    class OptionsBase
    {
      public:
        /**
         * @brief [brief description]
         * @details [long description]
         */
        OptionsBase();

        /**
         * @brief [brief description]
         * @details [long description]
         */
        virtual ~OptionsBase();

        /**
         * @brief [brief description]
         * @details [long description]
         */
        void setLabel(const std::string &label);

        /**
         * @brief [brief description]
         * @details [long description]
         */
        void setColour(const Colour &colour);

        /**
         * @brief [brief description]
         * @details [long description]
         */
        const std::string &getLabel() const;

        /**
         * @brief [brief description]
         * @details [long description]
         */
        const Colour &getColour() const;

      private:
        /// define the label for a PlotBase shown in Legend
        std::string label_;
        /// define Colour that is used plotting
        Colour colour_;
    };

}  // namespace scatter