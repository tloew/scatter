/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <string>

namespace scatter
{
    /**
     * @class FigureOptions Options.hpp <scatter/Options.hpp>
     * @brief [brief description]
     * @details [long description]
     */
    class FigureOptions
    {
      public:
        /**
         * @brief [brief description]
         * @details [long description]
         */
        FigureOptions();

        /**
         * @brief [brief description]
         * @details [long description]
         */
        ~FigureOptions();

        /**
         * @brief [brief description]
         * @details [long description]
         */
        FigureOptions &setTitle(const std::string &title);

        /**
         * @brief [brief description]
         * @details [long description]
         */
        FigureOptions &setWidth(const double &width);

        /**
         * @brief [brief description]
         * @details [long description]
         */
        FigureOptions &setHeight(const double &height);

        /**
         * @brief [brief description]
         * @details [long description]
         */
        const std::string &getTitle() const;

        /**
         * @brief [brief description]
         * @details [long description]
         */
        const double &getWidth() const;

        /**
         * @brief [brief description]
         * @details [long description]
         */
        const double &getHeight() const;

      private:
        /// title of the Figure
        std::string title_;
        /// width of the whole Figure when rendering
        double width_;
        /// height of the whole Figure when rendering
        double height_;
    };
}  // namespace scatter