/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <scatter/Colour.hpp>
#include <scatter/ColourSchemeSpringPastels.hpp>

namespace scatter
{

    ColourSchemeSpringPastels::ColourSchemeSpringPastels()
      : ColourScheme("spring_pastels", std::vector<Colour>({ Colour(0xFD7F6F),  //
                                                             Colour(0x7EB0D5),  //
                                                             Colour(0xC2C061),  //
                                                             Colour(0xBD7EBE),  //
                                                             Colour(0xFFB55A),  //
                                                             Colour(0xFFEE65),  //
                                                             Colour(0xBEB9DB),  //
                                                             Colour(0xFDCCE5),  //
                                                             Colour(0x8BD3C7) }))
    {}

}  // namespace scatter