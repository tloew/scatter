/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <string>

namespace scatter
{
    /**
     * @class AxisOptions Options.hpp <scatter/Options.hpp>
     * @brief [brief description]
     * @details [long description]
     */
    class AxisOptions
    {
      public:
        /**
         * @brief [brief description]
         * @details [long description]
         */
        AxisOptions();

        /**
         * @brief [brief description]
         * @details [long description]
         */
        ~AxisOptions();

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param show [description]
         */
        AxisOptions &setShow(const bool &show);

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param show_grid [description]
         */
        AxisOptions &setShowGrid(const bool &show_grid);

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param xmin [description]
         */
        AxisOptions &setXmin(const double &xmin);

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param xmax [description]
         */
        AxisOptions &setXmax(const double &xmax);

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param xticks [description]
         */
        AxisOptions &setXticks(const double &xticks);

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param show_xticks [description]
         */
        AxisOptions &setShowXticks(const bool &show_xticks);

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param xlabel [description]
         */
        AxisOptions &setXlabel(const std::string &xlabel);

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param ymin [description]
         */
        AxisOptions &setYmin(const double &ymin);

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param ymax [description]
         */
        AxisOptions &setYmax(const double &ymax);

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param yticks [description]
         */
        AxisOptions &setYticks(const double &yticks);

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param show_yticks [description]
         */
        AxisOptions &setShowYticks(const bool &show_yticks);

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param ylabel [description]
         */
        AxisOptions &setYlabel(const std::string &ylabel);

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param precision [description]
         */
        AxisOptions &setXPrecision(const int &precision);

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param precision [description]
         */
        AxisOptions &setYPrecision(const int &precision);

        AxisOptions &setYLogScale(const bool &y_log_scale);

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        const bool &getShow() const;

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        const bool &getShowGrid() const;

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        const double &getXmin() const;

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        const double &getXmax() const;

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        const double &getXticks() const;

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        const bool &getShowXticks() const;

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        const std::string &getXlabel() const;

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        const double &getYmin() const;

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        const double &getYmax() const;

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        const double &getYticks() const;

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        const bool &getShowYticks() const;

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        const std::string &getYlabel() const;

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        const int &getXPrecision() const;

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        const int &getYPrecision() const;

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        const bool &getYLogScale() const;

      private:
        ///
        bool show_;
        /// true: draw grid lines
        bool show_grid_;
        ///
        double xmin_;
        ///
        double xmax_;
        ///
        double xticks_;
        ///
        bool show_xticks_;
        /// label of the x-Axis
        std::string xlabel_;
        ///
        double ymin_;
        ///
        double ymax_;
        ///
        double yticks_;
        ///
        bool show_yticks_;
        /// label of the y-Axis
        std::string ylabel_;
        ///
        int x_precision_;
        ///
        int y_precision_;
        ///
        bool y_log_scale_;
    };
}  // namespace scatter