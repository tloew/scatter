/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <scatter/Options.hpp>

namespace scatter
{
    OptionsBase::OptionsBase()
    {
        label_ = "";
        colour_ = Colour();
    }

    OptionsBase::~OptionsBase() {}

    void OptionsBase::setLabel(const std::string &label)
    {
        label_ = label;
    }

    void OptionsBase::setColour(const Colour &colour)
    {
        colour_ = colour;
    }

    const std::string &OptionsBase::getLabel() const
    {
        return label_;
    }

    const Colour &OptionsBase::getColour() const
    {
        return colour_;
    }

}  // namespace scatter