/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <scatter/Point.hpp>

namespace scatter
{
    Point::Point(const double &x, const double &y) : x_(x), y_(y) {}

    const double &Point::x() const
    {
        return x_;
    }

    const double &Point::y() const
    {
        return y_;
    }

    Point Point::operator-(const Point &point) const
    {
        double x = x_ - point.x();
        double y = y_ - point.y();

        return Point(x, y);
    }

    Point Point::operator+(const Point &point) const
    {
        double x = x_ + point.x();
        double y = y_ + point.y();

        return Point(x, y);
    }

    Point Point::operator*(const double &multiplier) const
    {
        double x = multiplier * x_;
        double y = multiplier * y_;

        return Point(x, y);
    }

    Point Point::operator/(const double &value) const
    {
        double x = x_ / value;
        double y = y_ / value;

        return Point(x, y);
    }

    Point operator*(const double &multiplier, const Point &point)
    {
        return point * multiplier;
    }

}  // namespace scatter