/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <scatter/Figure.hpp>
#include <scatter/Plot.hpp>
#include <scatter/Point.hpp>
#include <scatter/render/CairoRenderer.hpp>
#include <scatter/render/Transform.hpp>

namespace scatter
{
    Figure::Figure(const FigureOptions &options, const unsigned &n_rows, const unsigned &n_cols) : options_(options), n_rows_(n_rows), n_cols_(n_cols)
    {
        plots_ = std::vector<std::vector<std::shared_ptr<Plot>>>(n_rows);
        for (unsigned i = 0; i < n_rows; ++i)
        {
            plots_[i] = std::vector<std::shared_ptr<Plot>>(n_cols);
        }
    }

    Figure::~Figure() {}

    void Figure::save(const std::string &file)
    {
        CairoRenderer renderer(file, options_.getWidth(), options_.getHeight());

        for (unsigned i = 0; i < n_rows_; ++i)
        {
            for (unsigned j = 0; j < n_cols_; ++j)
            {
                if (plots_[i][j])
                {
                    double w = options_.getWidth() / static_cast<double>(n_cols_);
                    double h = options_.getHeight() / static_cast<double>(n_rows_);
                    double x = static_cast<double>(j) * w;
                    double y = static_cast<double>(i) * h;

                    double char_width = renderer.getCharWidth(TextOptions());

                    if (n_cols_ > 1)
                    {
                        // w -= char_width;
                        // x += static_cast<double>(j) * char_width;
                    }

                    if (n_rows_ > 1)
                    {
                        h -= char_width;
                        y += static_cast<double>(i) * char_width;
                    }

                    plots_[i][j]->render(renderer, Point(x, y), w, h);
                }
            }
        }
    }

    void Figure::add(const std::shared_ptr<Plot> &plot, const unsigned &row, const unsigned &col)
    {
        if (row < n_rows_ && col < n_cols_)
        {
            plots_[row][col] = plot;
        }
    }

}  // namespace scatter
