/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <scatter/Colour.hpp>

namespace scatter
{
    Colour::Colour() : Colour(0.0, 0.0, 0.0, 0.0) {}

    Colour::Colour(const double &r, const double &g, const double &b, const double &a) : Colour(std::array<double, 4>({ r, g, b, a })) {}

    Colour::Colour(const int &r, const int &g, const int &b, const int &a)
      : Colour(static_cast<double>(r) / 255.0, static_cast<double>(g) / 255.0, static_cast<double>(b) / 255.0, static_cast<double>(a) / 255.0)
    {}

    Colour::Colour(const std::array<double, 4> &c)
    {
        for (unsigned i = 0; i < 4; ++i)
        {
            if (c[i] < 0.0)
            {
                data_[i] = 0.0;
            }
            else if (c[i] > 1.0)
            {
                data_[i] = 1.0;
            }
            else
            {
                data_[i] = c[i];
            }
        }
    }

    Colour::Colour(const int &hex) : Colour(createFromHex(hex)) {}

    Colour::~Colour() {}

    const double &Colour::r() const
    {
        return data_[0];
    }

    const double &Colour::g() const
    {
        return data_[1];
    }

    const double &Colour::b() const
    {
        return data_[2];
    }

    const double &Colour::a() const
    {
        return data_[3];
    }

    bool Colour::operator==(const Colour &other) const
    {
        return (r() == other.r()) &&  //
               (g() == other.g()) &&  //
               (b() == other.b()) &&  //
               (a() == other.a());
    }

    Colour Colour::red = Colour(1.0, 0.0, 0.0, 1.0);
    Colour Colour::green = Colour(0.0, 1.0, 0.0, 1.0);
    Colour Colour::blue = Colour(0.0, 0.0, 1.0, 1.0);
    Colour Colour::yellow = Colour(1.0, 1.0, 0.0, 1.0);
    Colour Colour::black = Colour(0.0, 0.0, 0.0, 1.0);
    Colour Colour::white = Colour(1.0, 1.0, 1.0, 1.0);
    Colour Colour::cyan = Colour(0.0, 1.0, 1.0, 1.0);
    Colour Colour::pink = Colour(1.0, 0.41, 0.7, 1.0);

    Colour Colour::mix(const double &weight, const Colour &c1, const Colour &c2)
    {
        double w2 = std::max(std::min(weight, 1.0), 0.0);
        double w1 = 1.0 - w2;

        return Colour(w1 * c1.r() + w2 * c2.r(),  //
                      w1 * c1.g() + w2 * c2.g(),  //
                      w1 * c1.b() + w2 * c2.b(),  //
                      w1 * c1.a() + w2 * c2.a());
    }

    Colour Colour::createFromHex(const int &hex)
    {
        int r = 0;
        int g = 0;
        int b = 0;
        int a = 255;

        if (hex > 0xFFFFFF)
        {
            r = ((hex >> 24) & 0xFF);
            g = ((hex >> 16) & 0xFF);
            b = ((hex >> 8) & 0xFF);
            a = ((hex >> 0) & 0xFF);
        }
        else
        {
            r = ((hex >> 16) & 0xFF);
            g = ((hex >> 8) & 0xFF);
            b = ((hex >> 0) & 0xFF);
        }

        return Colour(r, g, b, a);
    }

}  // namespace scatter