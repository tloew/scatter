/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <functional>
#include <memory>
#include <scatter/Figure.hpp>
#include <scatter/PlotOptions.hpp>
#include <string>

namespace scatter
{
    // forward declarations
    class Renderer;
    class Transform;
    class Figure;
    class Point;
    class Ellipse;
    class Arrow;
    class PlotBase;
    class Axis;
    class Legend;

    /**
     * @class Plot Plot.hpp <scatter/Plot.hpp>
     * @brief main class for creating plots
     * @details instances of this class can only be created using Plot::create
     */
    class Plot : public std::enable_shared_from_this<Plot>
    {
      public:
        /**
         * @brief delete default constructor in order to enforce using Plot::create
         * @details [long description]
         */
        Plot() = delete;

        /**
         * @brief destructor
         * @details [long description]
         */
        virtual ~Plot();

        /**
         * @brief save the plot to the given filepath
         * @details this function creates a temporary Figure
         *
         * @param file path and file name
         * @param options options for the temporary Figure that is created in order to save the Plot
         * @return true if the Plot was saved successfully
         */
        bool save(const std::string &file, const FigureOptions &options = FigureOptions());

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        PlotOptions &options();

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param  args [description]
         * @return [description]
         */
        template <class T, class... Args>
        typename T::Options *add(Args &&...args)
        {
            plots_.push_back(std::make_unique<T>(args...));

            return dynamic_cast<T *>(plots_.back().get())->options();
        }

      private:
        /// allow Figure::save to access private members of this class
        friend void Figure::save(const std::string &);

        /**
         * @brief constructor
         * @details private  in order to enforce using Plot::create
         *
         * @param options PlotOptions
         */
        Plot(const std::string &title, const PlotOptions &options);

        /**
         * @brief render the plot using the given Renderer
         * @details [long description]
         *
         * @param renderer Renderer (base to inherit from)
         * @param origin origin of the Plot in Renderer coordinates
         * @param width allocated width for the Plot
         * @param height allocated height for the Plot
         */
        void render(Renderer &renderer, const Point &origin, const double &width, const double &height);

      private:
        /// all plots to be rendered
        std::vector<std::unique_ptr<PlotBase>> plots_;

        ///
        std::unique_ptr<Axis> axis_;

        ///
        std::unique_ptr<Legend> legend_;

        ///
        PlotOptions options_;

      public:
        /// typedef for convenience
        typedef std::shared_ptr<Plot> Ptr;

        /**
         * @brief create a new Plot
         * @details [long description]
         *
         * @param options PlotOptions
         * @return Plot
         */
        static Plot::Ptr create(const PlotOptions &options = PlotOptions());

        /**
         * @brief create a new Plot
         * @details convenience overload
         *
         * @param title title of the Plot
         * @param options PlotOptions
         * @return Plot
         */
        static Plot::Ptr create(const std::string &title, const PlotOptions &options = PlotOptions());
    };

}  // namespace scatter