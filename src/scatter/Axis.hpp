/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <scatter/AxisOptions.hpp>

namespace scatter
{
    class Renderer;
    class Transform;
    class TextOptions;

    /**
     * @brief [brief description]
     * @details private class, not meant to be used by the user
     */
    class Axis
    {
      public:
        /**
         * @brief constructor
         * @details this class is instantiated within Plot
         *
         * @param options pointer to AxisOptions (coming from Plot)
         * @param text_options pointer to TextOptions (coming from Plot)
         */
        Axis(const AxisOptions *const options, const TextOptions *const text_options);

        /**
         * @brief destructor
         * @details [long description]
         */
        virtual ~Axis();

        /**
         * @brief render the Axis using the given Renderer
         * @details [long description]
         *
         * @param renderer Renderer (base class)
         * @param transform Transform to convert from data to canvas coordinates
         */
        void render(Renderer &renderer, const Transform &transform);

      protected:
      private:
        /// pointer to AxisOptions (coming from Plot)
        const AxisOptions *const options_;

        /// pointer to TextOptions (coming from Plot)
        const TextOptions *const text_options_;
    };

}  // namespace scatter