/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <scatter/FigureOptions.hpp>

namespace scatter
{

    FigureOptions::FigureOptions()
    {
        title_ = "Figure";
        width_ = 1000;
        height_ = 1000;
    }

    FigureOptions::~FigureOptions() {}

    FigureOptions &FigureOptions::setTitle(const std::string &title)
    {
        title_ = title;

        return *this;
    }

    FigureOptions &FigureOptions::setWidth(const double &width)
    {
        width_ = width;

        return *this;
    }

    FigureOptions &FigureOptions::setHeight(const double &height)
    {
        height_ = height;

        return *this;
    }

    const std::string &FigureOptions::getTitle() const
    {
        return title_;
    }

    const double &FigureOptions::getWidth() const
    {
        return width_;
    }

    const double &FigureOptions::getHeight() const
    {
        return height_;
    }

}  // namespace scatter