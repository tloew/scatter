/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <scatter/Arrow.hpp>
#include <scatter/Legend.hpp>
#include <scatter/render/Renderer.hpp>
#include <scatter/render/Transform.hpp>

namespace scatter
{
    Legend::Legend(const LegendOptions *const options, const TextOptions *const text_options)
      : options_(options), text_options_(text_options), max_chars_(0)
    {}

    Legend::~Legend() {}

    void Legend::add(const Entry &entry)
    {
        entries_.push_back(entry);

        if (entry.label.size() > max_chars_)
        {
            max_chars_ = static_cast<unsigned>(entry.label.size());
        }
    }

    void Legend::render(Renderer &renderer, const Transform &transform)
    {
        if (!options_->getShow())
        {
            return;
        }

        double text_height = text_options_->getSize();
        double height = 1.5 * text_height * static_cast<double>(entries_.size());
        double symbol_width = 5.0 * renderer.getCharWidth(*text_options_);
        double width = symbol_width + (1.15 * static_cast<double>(max_chars_) * renderer.getCharWidth(*text_options_));

        Point corner(0.0, 0.0);

        switch (options_->getAnchor())
        {
        case Anchor::NORTH_WEST:
            corner = transform.relative(0.0, 1.0) + Point(15.0, 15.0);
            break;
        case Anchor::CENTER:
            corner = transform.relative(0.5, 0.5) - Point(width / 2.0, height / 2.0);
            break;
        case Anchor::NORTH:
            corner = transform.relative(0.5, 1.0) + Point(0.0, 10.0) - Point(width / 2.0, 0.0);
            break;
        case Anchor::NORTH_EAST:
            corner = transform.relative(1.0, 1.0) + Point(-10.0, 10.0) - Point(width, 0.0);
            break;
        case Anchor::EAST:
            corner = transform.relative(1.0, 0.5) + Point(-10.0, 0.0) - Point(width, 0.0);
            break;
        case Anchor::SOUTH_EAST:
            corner = transform.relative(1.0, 0.0) + Point(-10.0, -10.0) - Point(width, height);
            break;
        case Anchor::SOUTH:
            corner = transform.relative(0.5, 0.0) + Point(0.0, -10.0) - Point(width / 2.0, height);
            break;
        case Anchor::SOUTH_WEST:
            corner = transform.relative(0.0, 0.0) + Point(10.0, -10.0) - Point(0.0, height);
            break;
        case Anchor::WEST:
            corner = transform.relative(0.0, 0.5) + Point(10.0, 0.0) - Point(0.0, height / 2.0);
            break;
        }

        renderer.rectangle(corner, corner + Point(width, height), Colour(1.0, 1.0, 1.0, 1.0));

        if (options_->getBoxed())
        {
            renderer.rectangle(corner, corner + Point(width, height), Colour(0.0, 0.0, 0.0, 1.0), false);
        }

        int i = 0;
        for (const Entry &entry : entries_)
        {
            if (entry.label.empty())
            {
                continue;
            }

            Point p = corner + Point(0, text_height / 2.0 + 1.5 * static_cast<double>(i++) * text_height);

            TextOptions text_options = *text_options_;
            text_options.setAnchor(Anchor::WEST);

            renderer.text(p + Point(symbol_width, 0.0), entry.label, text_options);

            if (auto val = std::get_if<Symbol>(&entry.symbol))
            {
                switch (*val)
                {
                case Symbol::LINE:
                    renderer.line(p + Point(0.25 * symbol_width, 0.0), p + Point(0.75 * symbol_width, 0.0), entry.colour, 7.5);
                    break;
                case Symbol::BAR:
                    renderer.rectangle(p + Point(0.25 * symbol_width, -7.5), p + Point(0.75 * symbol_width, 7.5), entry.colour);
                    break;
                case Symbol::ELLIPSE:
                    renderer.ellipse(p + Point(0.5 * symbol_width, 0.0), 0.2 * text_height, 0.3 * symbol_width, 0.0, entry.colour, false);
                    break;
                case Symbol::ARROW:
                    Arrow arrow = Arrow(p + Point(0.25 * symbol_width, 0.0), p + Point(0.75 * symbol_width, 0.0));
                    renderer.arrow(arrow.getPoints()[0], arrow.getPoints()[1], arrow.getPoints()[2], arrow.getPoints()[3], entry.colour);
                    break;
                }
            }
            else if (auto val = std::get_if<Marker>(&entry.symbol))
            {
                switch (*val)
                {
                case Marker::CIRCLE:
                    renderer.circle(p + Point(0.5 * symbol_width, 0.0), 2.5, entry.colour, true);
                    break;
                case Marker::ASTERISK:
                    renderer.asterisk(p + Point(0.5 * symbol_width, 0.0), 2.5, entry.colour);
                    break;
                case Marker::CROSS:
                    renderer.cross(p + Point(0.5 * symbol_width, 0.0), 2.5, entry.colour);
                    break;
                case Marker::PLUS:
                    renderer.plus(p + Point(0.5 * symbol_width, 0.0), 2.5, entry.colour);
                    break;
                case Marker::TRIANGLE:
                    renderer.triangle(p + Point(0.5 * symbol_width, 0.0), 2.5, entry.colour, true);
                    break;
                case Marker::DIAMOND:
                    renderer.diamond(p + Point(0.5 * symbol_width, 0.0), 2.5, entry.colour, true);
                    break;
                case Marker::SQUARE:
                    renderer.square(p + Point(0.5 * symbol_width, 0.0), 2.5, entry.colour, true);
                    break;
                case Marker::STAR:
                    renderer.star(p + Point(0.5 * symbol_width, 0.0), 2.5, entry.colour, true);
                    break;
                }
            }
        }
    }

}  // namespace scatter