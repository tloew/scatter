/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <scatter/ColourSchemeSpringPastels.hpp>
#include <scatter/PlotOptions.hpp>

namespace scatter
{
    PlotOptions::PlotOptions()
    {
        title_.setSize(25.0);
        title_.setBold(true);
        title_.setAnchor(Anchor::NORTH);

        colour_scheme_ = std::make_shared<ColourSchemeSpringPastels>();
    }

    PlotOptions::~PlotOptions() {}

    AxisOptions &PlotOptions::getAxisOptions()
    {
        return axis_;
    }

    LegendOptions &PlotOptions::getLegendOptions()
    {
        return legend_;
    }

    TextOptions &PlotOptions::getTitleOptions()
    {
        return title_;
    }

    TextOptions &PlotOptions::getTextOptions()
    {
        return text_;
    }

    const AxisOptions &PlotOptions::getAxisOptions() const
    {
        return axis_;
    }

    const LegendOptions &PlotOptions::getLegendOptions() const
    {
        return legend_;
    }

    const TextOptions &PlotOptions::getTitleOptions() const
    {
        return title_;
    }

    const TextOptions &PlotOptions::getTextOptions() const
    {
        return text_;
    }

    void PlotOptions::setTitle(const std::string &title_text)
    {
        title_text_ = title_text;
    }

    const std::string &PlotOptions::getTitle() const
    {
        return title_text_;
    }

    const ColourScheme &PlotOptions::getColourScheme() const
    {
        return *colour_scheme_.get();
    }

}  // namespace scatter