/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

namespace scatter
{
    /**
     * @brief [brief description]
     * @details [long description]
     */
    class Point
    {
      public:
        /**
         * @brief constructor
         * @details [long description]
         *
         * @param x x value
         * @param y y value
         */
        Point(const double &x, const double &y);

        /**
         * @brief return the x value
         * @details [long description]
         *
         * @return x value
         */
        const double &x() const;

        /**
         * @brief return the y value
         * @details [long description]
         *
         * @return x value
         */
        const double &y() const;

        /**
         * @brief substract another Point
         * @details [long description]
         *
         * @param point Point
         * @return difference between this and the other Point
         */
        Point operator-(const Point &point) const;

        /**
         * @brief add another Point
         * @details [long description]
         *
         * @param point Point
         * @return sum of this and the other Point
         */
        Point operator+(const Point &point) const;

        /**
         * @brief multiply this Point by the given value
         * @details [long description]
         *
         * @param multiplier value
         * @return this Point mulitplied by value
         */
        Point operator*(const double &multiplier) const;

        /**
         * @brief divide this Point by the given value
         * @details [long description]
         *
         * @param value divide value
         * @return this Point divided by value
         */
        Point operator/(const double &value) const;

      private:
        /// x coordinate
        double x_;
        /// y coordinate
        double y_;
    };

    /**
     * @brief multiply a Point by the given value
     * @details [long description]
     *
     * @param multiplier value
     * @param point Point
     *
     * @return the Point mulitplied by value
     */
    Point operator*(const double &multiplier, const Point &point);

}  // namespace scatter