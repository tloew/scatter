/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <memory>
#include <scatter/FigureOptions.hpp>
#include <string>
#include <vector>

namespace scatter
{
    /// forward declaration
    class Plot;

    /**
     * @class Figure Figure.hpp <scatter/Figure.hpp>
     * @brief [brief description]
     * @details [long description]
     */
    class Figure
    {
      public:
        /**
         * @brief constructor
         * @details [long description]
         *
         * @param options FigureOptions
         * @param n_rows number of rows for Plot
         * @param n_cols number of columns for Plot
         */
        Figure(const FigureOptions &options = FigureOptions(), const unsigned &n_rows = 1, const unsigned &n_cols = 1);

        /**
         * @brief destructor
         * @details [long description]
         */
        virtual ~Figure();

        /**
         * @brief add a Plot to this Figure
         * @details will override the Plot that is currently at the given position!
         *
         * @param plot Plot
         * @param row assign a row to the given Plot
         * @param col assign a column to the given Plot
         */
        void add(const std::shared_ptr<Plot> &plot, const unsigned &row = 0, const unsigned &col = 0);

        /**
         * @brief save the Figure to the given file
         * @details [long description]
         *
         * @param file filepath
         */
        void save(const std::string &file);

      protected:
      private:
        /// FigureOptions
        const FigureOptions options_;

        /// number of rows for Plot
        unsigned n_rows_;
        /// number of columns for Plot
        unsigned n_cols_;

        /// store all Plot in this Figure
        std::vector<std::vector<std::shared_ptr<Plot>>> plots_;
    };

}  // namespace scatter