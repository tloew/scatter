/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <scatter/Colour.hpp>
#include <scatter/Point.hpp>
#include <scatter/plots/LinePlot.hpp>
#include <scatter/plots/LinePlotOptions.hpp>
#include <scatter/render/Renderer.hpp>
#include <scatter/render/Transform.hpp>

namespace scatter
{
    LinePlot::LinePlot(const std::string &label, const std::vector<Point> &points, const Options &options)
      : PlotBase(label), points_(points), options_(std::make_unique<Options>(options))
    {}

    LinePlot::~LinePlot() {}

    void LinePlot::render(Renderer &renderer, const Transform &transform)
    {
        for (unsigned i = 1; i < points_.size(); ++i)
        {
            renderer.line(transform(points_[i - 1]), transform(points_[i]), getColour(), options_->getThickness());
        }
    }

    Legend::Entry LinePlot::getLegendEntry()
    {
        Legend::Entry entry;

        entry.label = getLabel();
        entry.colour = getColour();
        entry.symbol = Legend::Symbol::LINE;

        return entry;
    }

    LinePlot::Options *LinePlot::options()
    {
        return options_.get();
    }

}  // namespace scatter