/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <scatter/Options.hpp>
#include <scatter/plots/ColourGridPlot.hpp>

namespace scatter
{

    /**
     * @class ColourGridPlot::Options ColourGridPlotOptions.hpp <scatter/plots/ColourGridPlotOptions.hpp>
     * @brief [brief description]
     * @details [long description]
     */
    class ColourGridPlot::Options
    {
      public:
        /**
         * @brief [brief description]
         * @details [long description]
         */
        Options();

        /**
         * @brief [brief description]
         * @details [long description]
         */
        virtual ~Options();

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param xmin [description]
         */
        Options &setXmin(const double &xmin);

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param xmax [description]
         */
        Options &setXmax(const double &xmax);

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param ymin [description]
         */
        Options &setYmin(const double &ymin);

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param ymax [description]
         */
        Options &setYmax(const double &ymax);

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param colour_low [description]
         */
        Options &setColourLow(const Colour &colour_low);

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param colour_high [description]
         */
        Options &setColourHigh(const Colour &colour_high);

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        const double &getXmin() const;

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        const double &getXmax() const;

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        const double &getYmin() const;

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        const double &getYmax() const;

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        const Colour &getColourLow() const;

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        const Colour &getColourHigh() const;

      private:
        ///
        double xmin_;
        ///
        double xmax_;
        ///
        double ymin_;
        ///
        double ymax_;
        ///
        Colour colour_low_;
        ///
        Colour colour_high_;
    };

}  // namespace scatter