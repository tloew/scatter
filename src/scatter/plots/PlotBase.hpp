/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <scatter/Legend.hpp>
#include <scatter/Plot.hpp>

namespace scatter
{
    /// forward declarations
    class Renderer;
    class Transform;

    /**
     * @brief [brief description]
     * @details [long description]
     */
    class PlotBase
    {
      public:
        /**
         * @brief [brief description]
         * @details [long description]
         */
        PlotBase(const std::string &label);

        /**
         * @brief [brief description]
         * @details [long description]
         */
        virtual ~PlotBase() = default;

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param renderer [description]
         * @param transform [description]
         */
        virtual void render(Renderer &renderer, const Transform &transform) = 0;

        /**
         * @brief create LegendEntry
         * @details defines symbol, label, colour
         * @return LegendEntry
         */
        virtual Legend::Entry getLegendEntry() = 0;

        /**
         * @brief [brief description]
         * @details [long description]
         */
        void setLabel(const std::string &label);

        /**
         * @brief [brief description]
         * @details [long description]
         */
        void setColour(const Colour &colour);

        /**
         * @brief [brief description]
         * @details [long description]
         */
        const std::string &getLabel() const;

        /**
         * @brief [brief description]
         * @details [long description]
         */
        const Colour &getColour() const;

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        virtual double getYmax();

      protected:
      private:
        ///
        std::string label_;

        ///
        Colour colour_;
    };

}  // namespace scatter