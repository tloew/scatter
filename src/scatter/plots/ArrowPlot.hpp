/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <scatter/Options.hpp>
#include <scatter/plots/PlotBase.hpp>
#include <variant>
#include <vector>

namespace scatter
{
    /// forward declarations
    class Point;
    class Arrow;

    /**
     * @brief [brief description]
     * @details [long description]
     */
    class ArrowPlot : public PlotBase
    {
      public:
        ///
        class Options;

      public:
        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param label [description]
         * @param arrows [description]
         * @param options [description]
         */
        ArrowPlot(const std::string &label, const std::vector<Arrow> &arrows, const Options &options);

        /**
         * @brief constructor
         * @details [long description]
         *
         * @param arrows array of Arrow
         * @param options Options
         */
        ArrowPlot(const std::vector<Arrow> &arrows, const Options &options);

        /**
         * @brief destuctor
         * @details [long description]
         */
        virtual ~ArrowPlot();

        /**
         * @brief create LegendEntry
         * @details defines symbol, label, colour
         * @return LegendEntry
         */
        Legend::Entry getLegendEntry();

        /**
         * @brief render the ArrowPlot
         * @details [long description]
         *
         * @param renderer Renderer (base class)
         * @param transform Transform
         */
        void render(Renderer &renderer, const Transform &transform);

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        Options *options();

      private:
        /// array of Arrow
        std::vector<Arrow> arrows_;
        /// Options
        std::unique_ptr<Options> options_;

      public:
        /// convenience typedef
        typedef std::unique_ptr<ArrowPlot> Ptr;
    };

}  // namespace scatter