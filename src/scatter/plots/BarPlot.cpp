/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <scatter/Colour.hpp>
#include <scatter/Point.hpp>
#include <scatter/plots/BarPlot.hpp>
#include <scatter/plots/BarPlotOptions.hpp>
#include <scatter/render/Renderer.hpp>
#include <scatter/render/Transform.hpp>

namespace scatter
{
    BarPlot::BarPlot(const std::string &label, const std::vector<Point> &points, const Options &options)
      : PlotBase(label), points_(points), options_(std::make_unique<Options>(options))
    {}

    BarPlot::BarPlot(const std::string &label, const std::vector<Point> &points) : BarPlot(label, points, Options()) {}

    BarPlot::BarPlot(const std::vector<Point> &points, const Options &options) : BarPlot("", points, options) {}

    BarPlot::~BarPlot() {}

    void BarPlot::render(Renderer &renderer, const Transform &transform)
    {
        for (const Point &point : points_)
        {
            renderer.rectangle(transform(point.x() - options_->getWidth() / 2.0, 0.0), transform(point.x() + options_->getWidth() / 2.0, point.y()),
                               getColour());

            if (options_->getBoxed())
            {
                renderer.rectangle(transform(point.x() - options_->getWidth() / 2.0, 0.0),
                                   transform(point.x() + options_->getWidth() / 2.0, point.y()), Colour(), false);
            }
        }
    }

    Legend::Entry BarPlot::getLegendEntry()
    {
        Legend::Entry entry;

        entry.label = getLabel();
        entry.colour = getColour();
        entry.symbol = Legend::Symbol::BAR;

        return entry;
    }

    BarPlot::Options *BarPlot::options()
    {
        return options_.get();
    }

    double BarPlot::getYmax()
    {
        return std::max_element(points_.begin(), points_.end(), [](const Point &lhs, const Point &rhs) { return lhs.y() > rhs.y(); })->y();
    }

}  // namespace scatter