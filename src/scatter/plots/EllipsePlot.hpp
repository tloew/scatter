/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <scatter/Options.hpp>
#include <scatter/plots/PlotBase.hpp>
#include <variant>
#include <vector>

namespace scatter
{
    class Point;
    class Ellipse;

    /**
     * @class EllipsePlot EllipsePlot.hpp <scatter/EllipsePlot.hpp>
     * @brief [brief description]
     * @details [long description]
     */
    class EllipsePlot : public PlotBase
    {
        ///
        class Options;

      public:
        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param ellipses [description]
         * @param options [description]
         */
        EllipsePlot(const std::string &label, const std::vector<Ellipse> &ellipses, const Options &options);

        /**
         * @brief [brief description]
         * @details [long description]
         */
        virtual ~EllipsePlot();

        /**
         * @brief create LegendEntry
         * @details defines symbol, label, colour
         * @return LegendEntry
         */
        Legend::Entry getLegendEntry();

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param renderer [description]
         * @param transform [description]
         */
        void render(Renderer &renderer, const Transform &transform);

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        Options *options();

      private:
        /// array of Ellipse
        std::vector<Ellipse> ellipses_;
        /// Options
        std::unique_ptr<Options> options_;

      public:
        /// convenience typedef
        using Ptr = std::unique_ptr<EllipsePlot>;
    };

}  // namespace scatter