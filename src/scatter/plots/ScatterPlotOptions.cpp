/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <scatter/plots/ScatterPlotOptions.hpp>

namespace scatter
{

    ScatterPlot::Options::Options()
    {
        marker_ = Marker::CIRCLE;
        size_ = 1.0;
    }

    ScatterPlot::Options::~Options() {}

    ScatterPlot::Options &ScatterPlot::Options::setMarker(const Marker &marker)
    {
        marker_ = marker;

        return *this;
    }

    ScatterPlot::Options &ScatterPlot::Options::setSize(const double &size)
    {
        size_ = size;

        return *this;
    }

    const Marker &ScatterPlot::Options::getMarker() const
    {
        return marker_;
    }

    const double &ScatterPlot::Options::getSize() const
    {
        return size_;
    }

}  // namespace scatter