/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <functional>
#include <scatter/plots/PlotBase.hpp>
#include <vector>

namespace scatter
{
    class Point;

    /**
     * @brief [brief description]
     * @details [long description]
     *
     * @param function [description]
     * @param options [description]
     *
     * @return [description]
     */
    class FunctionPlot : public PlotBase
    {
      public:
        class Options;

      public:
        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param x [description]
         * @param function [description]
         * @param options [description]
         */
        FunctionPlot(const std::string &label, const std::vector<double> &x, const std::function<double(const double &)> &function,
                     const Options &options);

        /**
         * @brief [brief description]
         * @details [long description]
         */
        virtual ~FunctionPlot();

        /**
         * @brief create LegendEntry
         * @details defines symbol, label, colour
         * @return LegendEntry
         */
        Legend::Entry getLegendEntry();

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param renderer [description]
         * @param transform [description]
         */
        void render(Renderer &renderer, const Transform &transform);

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        Options *options();

      private:
        ///
        std::vector<double> x_;

        ///
        std::function<double(const double &x)> function_;

        ///
        std::unique_ptr<Options> options_;

      private:
        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param x [description]
         * @return [description]
         */
        Point evaluate(const double &x);

      public:
        /// typedef
        using Ptr = std::unique_ptr<FunctionPlot>;
    };

}  // namespace scatter