/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <scatter/Arrow.hpp>
#include <scatter/Colour.hpp>
#include <scatter/plots/ArrowPlot.hpp>
#include <scatter/plots/ArrowPlotOptions.hpp>
#include <scatter/render/Renderer.hpp>
#include <scatter/render/Transform.hpp>

namespace scatter
{
    ArrowPlot::ArrowPlot(const std::string &label, const std::vector<Arrow> &arrows, const Options &options)
      : PlotBase(label), arrows_(arrows), options_(std::make_unique<Options>(options))
    {}

    ArrowPlot::ArrowPlot(const std::vector<Arrow> &arrow, const Options &options) : ArrowPlot("", arrow, options) {}

    ArrowPlot::~ArrowPlot() {}

    void ArrowPlot::render(Renderer &renderer, const Transform &transform)
    {
        for (const Arrow &ellipse : arrows_)
        {
            Arrow transformed_arrow = transform(ellipse);
            renderer.arrow(transformed_arrow.getPoints()[0], transformed_arrow.getPoints()[1], transformed_arrow.getPoints()[2],
                           transformed_arrow.getPoints()[3], getColour());
        }
    }

    Legend::Entry ArrowPlot::getLegendEntry()
    {
        Legend::Entry entry;

        entry.symbol = Legend::Symbol::ARROW;
        entry.label = getLabel();
        entry.colour = getColour();

        return entry;
    }

    ArrowPlot::Options *ArrowPlot::options()
    {
        return options_.get();
    }

}  // namespace scatter