/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <scatter/Colour.hpp>
#include <scatter/Ellipse.hpp>
#include <scatter/plots/EllipsePlot.hpp>
#include <scatter/plots/EllipsePlotOptions.hpp>
#include <scatter/render/Renderer.hpp>
#include <scatter/render/Transform.hpp>

namespace scatter
{
    EllipsePlot::EllipsePlot(const std::string &label, const std::vector<Ellipse> &ellipses, const Options &options)
      : PlotBase(label), ellipses_(ellipses), options_(std::make_unique<Options>(options))
    {}

    EllipsePlot::~EllipsePlot() {}

    void EllipsePlot::render(Renderer &renderer, const Transform &transform)
    {
        for (const Ellipse &ellipse : ellipses_)
        {
            Ellipse transformed_ellipse = transform(ellipse);
            renderer.ellipse(transformed_ellipse.getCenter(), transformed_ellipse.getMinor(), transformed_ellipse.getMajor(),
                             transformed_ellipse.getRotation(), getColour(), false);
        }
    }

    Legend::Entry EllipsePlot::getLegendEntry()
    {
        Legend::Entry entry;

        entry.symbol = Legend::Symbol::ELLIPSE;
        entry.label = getLabel();
        entry.colour = getColour();

        return entry;
    }

    EllipsePlot::Options *EllipsePlot::options()
    {
        return options_.get();
    }

}  // namespace scatter