/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <scatter/plots/HistogramPlot.hpp>

namespace scatter
{

    /**
     * @class HistogramPlot::Options HistogramPlotOptions.hpp <scatter/plots/HistogramPlotOptions.hpp>
     * @brief [brief description]
     * @details [long description]
     */
    class HistogramPlot::Options
    {
      public:
        /**
         * @brief [brief description]
         * @details [long description]
         */
        Options();

        /**
         * @brief [brief description]
         * @details [long description]
         */
        ~Options();

        /**
         * @brief [brief description]
         * @details [long description]
         */
        Options &setNbins(const int &nbins);

        /**
         * @brief [brief description]
         * @details [long description]
         */
        const int &getNbins() const;

      private:
        /// number of bins in HistogramPlot
        int nbins_;
    };

}  // namespace scatter