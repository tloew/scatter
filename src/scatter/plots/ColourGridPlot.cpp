/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <scatter/Colour.hpp>
#include <scatter/Point.hpp>
#include <scatter/plots/ColourGridPlot.hpp>
#include <scatter/plots/ColourGridPlotOptions.hpp>
#include <scatter/render/Renderer.hpp>
#include <scatter/render/Transform.hpp>

namespace scatter
{
    ColourGridPlot::ColourGridPlot(const std::string &label, const Eigen::MatrixXd &values, const Options &options)
      : PlotBase(label), values_(values), options_(std::make_unique<Options>(options))
    {}

    ColourGridPlot::ColourGridPlot(const std::string &label, const std::function<double(const double &, const double &)> &function, int resolution,
                                   const Options &options)
      : PlotBase(label), values_(evaluate(function, resolution, options)), options_(std::make_unique<Options>(options))
    {}

    ColourGridPlot::~ColourGridPlot() {}

    void ColourGridPlot::render(Renderer &renderer, const Transform &transform)
    {
        double min_val = values_.minCoeff();
        double max_val = values_.maxCoeff();

        std::cout << min_val << std::endl;
        std::cout << max_val << std::endl;
        std::cout << values_.sum() << std::endl;

        double dx = (options_->getXmax() - options_->getXmin()) / static_cast<double>(values_.cols());
        double dy = (options_->getYmax() - options_->getYmin()) / static_cast<double>(values_.rows());

        for (unsigned x = 0; x < values_.cols(); ++x)
        {
            for (unsigned y = 0; y < values_.rows(); ++y)
            {
                double xv = options_->getXmin() + static_cast<double>(x) * dx;
                double yv = options_->getYmin() + static_cast<double>(y) * dy;

                std::cout << (values_.coeff(y, x) - min_val) / (max_val - min_val) << " -> ";

                Colour colour =
                  Colour::mix((values_.coeff(y, x) - min_val) / (max_val - min_val), options_->getColourLow(), options_->getColourHigh());

                std::cout << colour.r() << " " << colour.g() << " " << colour.b() << std::endl;

                renderer.rectangle(transform(xv, yv), transform(xv + dx, yv + dy), colour);
            }
        }
    }

    Legend::Entry ColourGridPlot::getLegendEntry()
    {
        Legend::Entry entry;

        entry.symbol = Legend::Symbol::ARROW;
        entry.label = getLabel();
        entry.colour = getColour();

        return entry;
    }

    ColourGridPlot::Options *ColourGridPlot::options()
    {
        return options_.get();
    }

    Eigen::MatrixXd ColourGridPlot::evaluate(const std::function<double(const double &, const double &)> &function, int resolution,
                                             const Options &options)
    {
        double dx = (options.getXmax() - options.getXmin()) / static_cast<double>(resolution);
        double dy = (options.getYmax() - options.getYmin()) / static_cast<double>(resolution);

        Eigen::MatrixXd values = Eigen::MatrixXd::Zero(resolution, resolution);

        for (unsigned x = 0; x < resolution; ++x)
        {
            for (unsigned y = 0; y < resolution; ++y)
            {
                double xv = options.getXmin() + static_cast<double>(x) * dx;
                double yv = options.getYmin() + static_cast<double>(y) * dy;

                values.coeffRef(y, x) = function(xv, yv);
            }
        }

        std::cout << "here" << std::endl;

        return values;
    }

}  // namespace scatter