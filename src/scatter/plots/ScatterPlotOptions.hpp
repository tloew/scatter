/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <scatter/Options.hpp>
#include <scatter/plots/ScatterPlot.hpp>

namespace scatter
{

    /**
     * @class ScatterPlot::Options Options.hpp <scatter/Options.hpp>
     * @brief [brief description]
     * @details [long description]
     */
    class ScatterPlot::Options
    {
      public:
        /**
         * @brief [brief description]
         * @details [long description]
         */
        Options();

        /**
         * @brief [brief description]
         * @details [long description]
         */
        ~Options();

        /**
         * @brief [brief description]
         * @details [long description]
         */
        Options &setMarker(const Marker &marker);

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param size [description]
         */
        Options &setSize(const double &size);

        /**
         * @brief [brief description]
         * @details [long description]
         */
        const Marker &getMarker() const;

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        const double &getSize() const;

      private:
        /// define Marker to display in ScatterPlot
        Marker marker_;

        ///
        double size_;
    };

}  // namespace scatter