/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <scatter/plots/ContourPlotOptions.hpp>

namespace scatter
{

    ContourPlot::Options::Options() {}

    ContourPlot::Options::~Options() {}

    ContourPlot::Options &ContourPlot::Options::setColourLow(const Colour &colour_low)
    {
        colour_low_ = colour_low;

        return *this;
    }

    ContourPlot::Options &ContourPlot::Options::setColourHigh(const Colour &colour_high)
    {
        colour_high_ = colour_high;

        return *this;
    }

    ContourPlot::Options &ContourPlot::Options::setNumLevels(const int &num_levels)
    {
        num_levels_ = num_levels;

        return *this;
    }

    const int &ContourPlot::Options::getNumLevels() const
    {
        return num_levels_;
    }

    const Colour &ContourPlot::Options::getColourLow() const
    {
        return colour_low_;
    }

    const Colour &ContourPlot::Options::getColourHigh() const
    {
        return colour_high_;
    }

}  // namespace scatter