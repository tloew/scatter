/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <scatter/plots/PlotBase.hpp>
#include <vector>

namespace scatter
{
    /// forward declarations
    class Point;

    /**
     * @brief [brief description]
     * @details [long description]
     */
    class BarPlot : public PlotBase
    {
      public:
        ///
        class Options;

      public:
        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param label [description]
         * @param points [description]
         * @param options [description]
         */
        BarPlot(const std::string &label, const std::vector<Point> &points, const Options &options);

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param label [description]
         * @param points [description]
         */
        BarPlot(const std::string &label, const std::vector<Point> &points);

        /**
         * @brief constructor
         * @details [long description]
         *
         * @param points array of Point
         * @param options Options
         */
        BarPlot(const std::vector<Point> &points, const Options &options);

        /**
         * @brief destructor
         * @details [long description]
         */
        virtual ~BarPlot();

        /**
         * @brief create LegendEntry
         * @details defines symbol, label, colour
         * @return LegendEntry
         */
        Legend::Entry getLegendEntry();

        /**
         * @brief render the BarPlot
         * @details [long description]
         *
         * @param renderer Renderer (base class)
         * @param transform Transform
         */
        void render(Renderer &renderer, const Transform &transform);

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        Options *options();

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        double getYmax();

      private:
        /// array of Point
        std::vector<Point> points_;
        /// Options
        std::unique_ptr<Options> options_;

      public:
        /// convenience typedef
        typedef std::unique_ptr<BarPlot> Ptr;
    };

}  // namespace scatter