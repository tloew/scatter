/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <scatter/Colour.hpp>
#include <scatter/Point.hpp>
#include <scatter/plots/BoxPlot.hpp>
#include <scatter/plots/BoxPlotOptions.hpp>
#include <scatter/render/Renderer.hpp>
#include <scatter/render/Transform.hpp>

namespace scatter
{
    BoxPlot::BoxPlot(const std::string &label, const std::vector<std::vector<double>> &values, const Options &options)
      : PlotBase(label), values_(values), options_(std::make_unique<Options>(options))
    {}

    BoxPlot::~BoxPlot() {}

    void BoxPlot::render(Renderer &renderer, const Transform &transform)
    {
        for (std::vector<double> &values : values_)
        {
            std::sort(values.begin(), values.end());

            double min_value = values.front();
            double max_value = values.back();
            double median = (values.size() % 2 != 0) ? values[values.size() / 2] : (values[values.size() / 2] + values[values.size() / 2 - 1]) / 2.0;
            double lower = (values.size() % 2 != 0) ? values[values.size() / 4] : (values[values.size() / 4] + values[values.size() / 4 - 1]) / 2.0;
            double upper =
              (values.size() % 2 != 0) ? values[3 * values.size() / 4] : (values[3 * values.size() / 4] + values[3 * values.size() / 4 - 1]) / 2.0;

            renderer.rectangle(transform(Point(5.0, lower)), transform(Point(10.0, upper)), Colour(0.0, 1.0, 0.0, 1.0));
            renderer.rectangle(transform(Point(5.0, lower)), transform(Point(10.0, upper)), Colour(), false);
            renderer.line(transform(Point(5.0, median)), transform(Point(10.0, median)), Colour(1.0, 0.0, 0.0, 1.0));
            renderer.line(transform(Point(5.0, min_value)), transform(Point(10.0, min_value)), Colour(0.0, 0.0, 0.0, 1.0));
            renderer.line(transform(Point(5.0, max_value)), transform(Point(10.0, max_value)), Colour(0.0, 0.0, 0.0, 1.0));
            renderer.line(transform(Point(7.5, lower)), transform(Point(7.5, min_value)), Colour(0.0, 0.0, 0.0, 1.0));
            renderer.line(transform(Point(7.5, upper)), transform(Point(7.5, max_value)), Colour(0.0, 0.0, 0.0, 1.0));
        }
    }

    Legend::Entry BoxPlot::getLegendEntry()
    {
        Legend::Entry entry;

        entry.label = getLabel();
        entry.colour = getColour();
        entry.symbol = Legend::Symbol::BAR;

        return entry;
    }

    BoxPlot::Options *BoxPlot::options()
    {
        return options_.get();
    }

}  // namespace scatter