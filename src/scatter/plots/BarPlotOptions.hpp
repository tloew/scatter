/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <scatter/Options.hpp>
#include <scatter/plots/BarPlot.hpp>

namespace scatter
{
    /**

     * @class BarPlot::Options BarPlotOptions.hpp <scatter/plots/BarPlotOptions.hpp>
     * @brief [brief description]
     * @details [long description]
     */
    class BarPlot::Options
    {
      public:
        /**
         * @brief [brief description]
         * @details [long description]
         */
        Options();

        /**
         * @brief [brief description]
         * @details [long description]
         */
        ~Options();

        /**
         * @brief [brief description]
         * @details [long description]
         */
        Options &setWidth(const double &width);

        /**
         * @brief [brief description]
         * @details [long description]
         */
        Options &setBoxed(const bool &boxed);

        /**
         * @brief [brief description]
         * @details [long description]
         */
        const double &getWidth() const;

        /**
         * @brief [brief description]
         * @details [long description]
         */
        const bool &getBoxed() const;

      private:
        /// width of a bar in BarPlot
        double width_;
        /// true: black box around bar in BarPlot
        bool boxed_;
    };

}  // namespace scatter