/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <scatter/Options.hpp>
#include <scatter/plots/ContourPlot.hpp>

namespace scatter
{

    /**
     * @class ContourPlot::Options ContourPlot.hpp <scatter/plots/ContourPlot.hpp>
     * @brief [brief description]
     * @details [long description]
     */
    class ContourPlot::Options
    {
      public:
        /**
         * @brief [brief description]
         * @details [long description]
         */
        Options();

        /**
         * @brief [brief description]
         * @details [long description]
         */
        ~Options();

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param num_levels [description]
         */
        Options &setNumLevels(const int &num_levels);

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param colour_low [description]
         */
        Options &setColourLow(const Colour &colour_low);

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param colour_high [description]
         */
        Options &setColourHigh(const Colour &colour_high);

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        const int &getNumLevels() const;

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        const Colour &getColourLow() const;

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        const Colour &getColourHigh() const;

      private:
        ///
        int num_levels_ = 10;

        ///
        Colour colour_low_ = Colour(1.0, 0.0, 0.0, 1.0);
        ///
        Colour colour_high_ = Colour(0.5, 1.0, 0.0, 1.0);
    };

}  // namespace scatter