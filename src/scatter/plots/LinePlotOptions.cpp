/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <scatter/plots/LinePlotOptions.hpp>

namespace scatter
{

    LinePlot::Options::Options()
    {
        thickness_ = 0.1;
    }

    LinePlot::Options::~Options() {}

    LinePlot::Options &LinePlot::Options::setThickness(const double &thickness)
    {
        thickness_ = thickness;

        return *this;
    }

    const double &LinePlot::Options::getThickness() const
    {
        return thickness_;
    }

}  // namespace scatter