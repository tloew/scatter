/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <Eigen/Core>
//
#include <iostream>
#include <scatter/Colour.hpp>
#include <scatter/Point.hpp>
#include <scatter/plots/ContourPlot.hpp>
#include <scatter/plots/ContourPlotOptions.hpp>
#include <scatter/render/Renderer.hpp>
#include <scatter/render/Transform.hpp>

namespace scatter
{
    ContourPlot::ContourPlot(const std::string &label, const std::function<double(const double &x, const double &y)> &function,
                             const Options &options)
      : PlotBase(label), function_(function), options_(std::make_unique<Options>(options))
    {}

    ContourPlot::~ContourPlot() {}

    // double bilinearIneterpolate(const Eigen::MatrixXd &values, const Eigen::Vector2d &pt)
    // {
    //     int x = (int)pt.x();
    //     int y = (int)pt.y();

    //     const double distance_diff = distance1 - distance2;

    //     if (std::abs(distance_diff) >= kEpsilon)
    //     {
    //         return Position(vertex1 + (vertex2 - vertex1) * (distance1 / distance_diff));
    //     }
    //     else
    //     {
    //         return Position(0.5 * (vertex1 + vertex2));
    //     }

    //     double a = pt.x() - x;
    //     double c = pt.y() - y;

    //     return ((values.coeff(y0, x0) * (1.0 - a) + values.coeff(y0, x1) * a) * (1.0 - c) + (values.coeff(y1, x0) * (1.0 - a) + values.coeff(y1,
    //     x1) * a) * c);
    // }

    void ContourPlot::render(Renderer &renderer, const Transform &transform)
    {
        Eigen::VectorXd x_coords =
          Eigen::VectorXd::LinSpaced(1000, transform.getOptions().getAxisOptions().getXmin(), transform.getOptions().getAxisOptions().getXmax());
        Eigen::VectorXd y_coords =
          Eigen::VectorXd::LinSpaced(1000, transform.getOptions().getAxisOptions().getYmin(), transform.getOptions().getAxisOptions().getYmax());

        Eigen::MatrixXd values = Eigen::MatrixXd::NullaryExpr(
          1000, 1000, [this, &x_coords, &y_coords](Eigen::Index row, Eigen::Index col) { return function_(x_coords[col], y_coords[row]); });

        double min_val = values.minCoeff();
        double max_val = values.maxCoeff();
        double mean_val = values.mean();

        double dval = (max_val - mean_val) / (static_cast<double>(options_->getNumLevels()) / 2.0);
        Eigen::VectorXd levels = Eigen::VectorXd::Zero(options_->getNumLevels());

        if (options_->getNumLevels() > 1)
        {
            levels.block(0, 0, options_->getNumLevels() / 2, 1) = Eigen::VectorXd::LinSpaced(options_->getNumLevels() / 2, min_val, mean_val);
            levels.block(options_->getNumLevels() / 2, 0, options_->getNumLevels() / 2, 1) =
              Eigen::VectorXd::LinSpaced(options_->getNumLevels() / 2, mean_val + dval, max_val);
        }
        else
        {
            levels[0] = mean_val;
        }

        std::vector<Eigen::Vector2i> neighbours = { { 0, 0 }, { 0, 1 }, { 1, 1 }, { 1, 0 } };

        std::vector<Point> points = { Point(0.5, 0.0), Point(1.0, -0.5), Point(0.5, -1.0), Point(0.0, -0.5) };

        std::vector<std::vector<unsigned>> indices = {
            {},              //
            { 2, 3 },        //
            { 1, 2 },        //
            { 1, 3 },        //
            { 0, 1 },        //
            { 0, 3, 1, 2 },  //
            { 0, 2 },        //
            { 0, 3 },        //
            { 3, 0 },        //
            { 2, 0 },        //
            { 3, 2, 1, 0 },  //
            { 1, 0 },        //
            { 3, 1 },        //
            { 2, 1 },        //
            { 3, 2 },        //
            {},              //
        };

        for (unsigned i = static_cast<unsigned>(options_->getNumLevels()); i > 0; --i)
        {
            double val = levels[i - 1];

            Eigen::Matrix<bool, Eigen::Dynamic, Eigen::Dynamic> binary = (values.array() > val);

            for (int row = 0; row < values.rows() - 1; ++row)
            {
                for (int col = 0; col < values.cols() - 1; ++col)
                {
                    Eigen::Vector2i index(row, col);

                    uint8_t configuration = 0;

                    for (int j = 0; j < 4; ++j)
                    {
                        configuration |=
                          static_cast<uint8_t>(binary.coeff(row + neighbours[unsigned(j)][0], col + neighbours[unsigned(j)][1]) ? (1 << j) : 0);
                    }

                    std::vector<Point> contour;

                    for (unsigned j = 0; j < indices[configuration].size(); j += 2)
                    {
                        Point p0 = Point(x_coords[col], y_coords[row]);
                        Point p1 = p0 + (points[indices[configuration][j]]) * 6.0 / 1000.0;
                        Point p2 = p0 + (points[indices[configuration][j + 1]]) * 6.0 / 1000.0;

                        contour.push_back(transform(p1));
                        contour.push_back(transform(p2));

                        // double fraction = (get_point_z(point2) - level) /
                        //                       (get_point_z(point2) - get_point_z(point1));
                        // return get_point_xy(point1)*fraction + get_point_xy(point2)*(1.0 - fraction);
                    }

                    renderer.area(contour, Colour::mix(static_cast<double>(i) / static_cast<double>(options_->getNumLevels()),
                                                       options_->getColourLow(), options_->getColourHigh()));
                }
            }
        }
    }

    Legend::Entry ContourPlot::getLegendEntry()
    {
        Legend::Entry entry;

        entry.label = getLabel();
        entry.colour = getColour();
        entry.symbol = Legend::Symbol::LINE;

        return entry;
    }

    ContourPlot::Options *ContourPlot::options()
    {
        return options_.get();
    }

}  // namespace scatter