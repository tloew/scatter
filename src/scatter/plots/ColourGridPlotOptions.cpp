/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <scatter/plots/ColourGridPlotOptions.hpp>

namespace scatter
{

    ColourGridPlot::Options::Options() {}

    ColourGridPlot::Options::~Options() {}

    ColourGridPlot::Options &ColourGridPlot::Options::setXmin(const double &xmin)
    {
        xmin_ = xmin;

        return *this;
    }

    ColourGridPlot::Options &ColourGridPlot::Options::setXmax(const double &xmax)
    {
        xmax_ = xmax;

        return *this;
    }

    ColourGridPlot::Options &ColourGridPlot::Options::setYmin(const double &ymin)
    {
        ymin_ = ymin;

        return *this;
    }

    ColourGridPlot::Options &ColourGridPlot::Options::setYmax(const double &ymax)
    {
        ymax_ = ymax;

        return *this;
    }

    ColourGridPlot::Options &ColourGridPlot::Options::setColourLow(const Colour &colour_low)
    {
        colour_low_ = colour_low;

        return *this;
    }

    ColourGridPlot::Options &ColourGridPlot::Options::setColourHigh(const Colour &colour_high)
    {
        colour_high_ = colour_high;

        return *this;
    }

    const double &ColourGridPlot::Options::getXmin() const
    {
        return xmin_;
    }

    const double &ColourGridPlot::Options::getXmax() const
    {
        return xmax_;
    }

    const double &ColourGridPlot::Options::getYmin() const
    {
        return ymin_;
    }

    const double &ColourGridPlot::Options::getYmax() const
    {
        return ymax_;
    }

    const Colour &ColourGridPlot::Options::getColourLow() const
    {
        return colour_low_;
    }

    const Colour &ColourGridPlot::Options::getColourHigh() const
    {
        return colour_high_;
    }

}  // namespace scatter