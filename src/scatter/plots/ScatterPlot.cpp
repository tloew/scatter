/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <scatter/Colour.hpp>
#include <scatter/Point.hpp>
#include <scatter/plots/ScatterPlot.hpp>
#include <scatter/plots/ScatterPlotOptions.hpp>
#include <scatter/render/Renderer.hpp>
#include <scatter/render/Transform.hpp>

namespace scatter
{
    ScatterPlot::ScatterPlot(const std::string &label, const std::vector<Point> &points, const Options &options)
      : PlotBase(label), points_(points), options_(std::make_unique<Options>(options))
    {}

    ScatterPlot::~ScatterPlot() {}

    void ScatterPlot::render(Renderer &renderer, const Transform &transform)
    {
        for (const Point &point : points_)
        {
            switch (options_->getMarker())
            {
            case Marker::CIRCLE:
                renderer.circle(transform(point), options_->getSize(), getColour());
                break;
            case Marker::ASTERISK:
                renderer.asterisk(transform(point), options_->getSize(), getColour());
                break;
            case Marker::CROSS:
                renderer.cross(transform(point), options_->getSize(), getColour());
                break;
            case Marker::PLUS:
                renderer.plus(transform(point), options_->getSize(), getColour());
                break;
            case Marker::TRIANGLE:
                renderer.triangle(transform(point), options_->getSize(), getColour());
                break;
            case Marker::DIAMOND:
                renderer.diamond(transform(point), options_->getSize(), getColour());
                break;
            case Marker::SQUARE:
                renderer.square(transform(point), options_->getSize(), getColour());
                break;
            case Marker::STAR:
                renderer.star(transform(point), options_->getSize(), getColour());
                break;
            }
        }
    }

    Legend::Entry ScatterPlot::getLegendEntry()
    {
        Legend::Entry entry;

        entry.label = getLabel();
        entry.colour = getColour();
        entry.symbol = options_->getMarker();

        return entry;
    }

    ScatterPlot::Options *ScatterPlot::options()
    {
        return options_.get();
    }

}  // namespace scatter