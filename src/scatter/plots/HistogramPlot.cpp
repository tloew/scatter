/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <scatter/Colour.hpp>
#include <scatter/Point.hpp>
#include <scatter/plots/BarPlot.hpp>
#include <scatter/plots/BarPlotOptions.hpp>
#include <scatter/plots/HistogramPlot.hpp>
#include <scatter/plots/HistogramPlotOptions.hpp>
#include <scatter/render/Renderer.hpp>
#include <scatter/render/Transform.hpp>

namespace scatter
{
    HistogramPlot::HistogramPlot(const std::string &label, const std::vector<double> &values, const Options &options)
      : PlotBase(label), values_(values), options_(std::make_unique<Options>(options))
    {}

    HistogramPlot::~HistogramPlot() {}

    void HistogramPlot::render(Renderer &renderer, const Transform &transform)
    {
        std::sort(values_.begin(), values_.end());

        double min_value = values_.front();
        double max_value = values_.back();
        double difference = (max_value - min_value) / static_cast<double>(options_->getNbins());

        std::vector<Point> points;

        unsigned j = 0;
        for (int i = 1; i < options_->getNbins() + 1; ++i)
        {
            int count = 0;

            while (values_[j] < min_value + static_cast<double>(i) * difference)
            {
                ++count;
                ++j;
            }

            points.push_back(Point(min_value + (static_cast<double>(i) - 0.5) * difference, static_cast<double>(count)));
        }

        BarPlot::Options bar_options;
        bar_options.setWidth(difference);
        bar_options.setBoxed(true);

        BarPlot(points, bar_options).render(renderer, transform);
    }

    Legend::Entry HistogramPlot::getLegendEntry()
    {
        Legend::Entry entry;

        entry.label = getLabel();
        entry.colour = getColour();
        entry.symbol = Legend::Symbol::BAR;

        return entry;
    }

    HistogramPlot::Options *HistogramPlot::options()
    {
        return options_.get();
    }

}  // namespace scatter