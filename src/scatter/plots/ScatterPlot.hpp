/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <scatter/plots/PlotBase.hpp>
#include <vector>

namespace scatter
{
    class Point;

    /**
     * @class ScatterPlot ScatterPlot.hpp <scatter/ScatterPlot.hpp>
     * @brief
     * @details [long description]
     */
    class ScatterPlot : public PlotBase
    {
      public:
        class Options;

      public:
        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param points list of Point
         * @param options Options
         */
        ScatterPlot(const std::string &label, const std::vector<Point> &points, const Options &options);

        /**
         * @brief destructor
         * @details
         */
        virtual ~ScatterPlot();

        /**
         * @brief create LegendEntry
         * @details defines symbol, label, colour
         * @return LegendEntry
         */
        Legend::Entry getLegendEntry();

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param renderer [description]
         * @param transform [description]
         */
        void render(Renderer &renderer, const Transform &transform);

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        Options *options();

      private:
        /// list of Point
        std::vector<Point> points_;
        /// Options
        std::unique_ptr<Options> options_;

      public:
        /// typedef
        using Ptr = std::unique_ptr<ScatterPlot>;
    };

}  // namespace scatter