/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <scatter/Colour.hpp>
#include <scatter/Point.hpp>
#include <scatter/plots/FunctionPlot.hpp>
#include <scatter/plots/FunctionPlotOptions.hpp>
#include <scatter/render/Renderer.hpp>
#include <scatter/render/Transform.hpp>

namespace scatter
{
    FunctionPlot::FunctionPlot(const std::string &label, const std::vector<double> &x, const std::function<double(const double &x)> &function,
                               const Options &options)
      : PlotBase(label), x_(x), function_(function), options_(std::make_unique<Options>(options))
    {}

    FunctionPlot::~FunctionPlot() {}

    // FunctionPlot::Ptr FunctionPlot::create(const std::function<double(const double &x)> &function, const double &x_min, const double &x_max,
    //                                        const unsigned &steps)
    // {
    //     std::vector<double> x;

    //     double dx = (x_max - x_min) / (steps - 1);

    //     for (unsigned k = 0; k < steps; ++k)
    //     {
    //         x.push_back(x_min + static_cast<double>(k) * dx);
    //     }

    //     return create(x, function);
    // }

    // FunctionPlot::Ptr FunctionPlot::create(const std::vector<double> &x, const std::function<double(const double &x)> &function)
    // {
    //     return FunctionPlot::create(x, function, FunctionPlot::Options());
    // }

    // FunctionPlot::Ptr FunctionPlot::create(const std::vector<double> &x, const std::function<double(const double &x)> &function,
    //                                        const Options &options)
    // {
    //     return std::make_unique<FunctionPlot>(x, function, options);
    // }

    void FunctionPlot::render(Renderer &renderer, const Transform &transform)
    {
        for (unsigned i = 1; i < x_.size(); ++i)
        {
            Point y1 = evaluate(x_[i - 1]);
            Point y2 = evaluate(x_[i]);

            renderer.line(transform(y1), transform(y2), getColour());
        }
    }

    Point FunctionPlot::evaluate(const double &x)
    {
        return Point(x, function_(x));
    }

    Legend::Entry FunctionPlot::getLegendEntry()
    {
        Legend::Entry entry;

        entry.label = getLabel();
        entry.colour = getColour();
        entry.symbol = Legend::Symbol::LINE;

        return entry;
    }

    FunctionPlot::Options *FunctionPlot::options()
    {
        return options_.get();
    }

}  // namespace scatter