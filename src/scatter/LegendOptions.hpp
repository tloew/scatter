/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <scatter/Options.hpp>

namespace scatter
{
    /**
     * @class LegendOptions Options.hpp <scatter/Options.hpp>
     * @brief [brief description]
     * @details [long description]
     */
    class LegendOptions
    {
      public:
        /**
         * @brief [brief description]
         * @details [long description]
         */
        LegendOptions();

        /**
         * @brief [brief description]
         * @details [long description]
         */
        ~LegendOptions();

        /**
         * @brief [brief description]
         * @details [long description]
         */
        LegendOptions &setShow(const bool &show);

        /**
         * @brief [brief description]
         * @details [long description]
         */
        LegendOptions &setAnchor(const Anchor &anchor);

        /**
         * @brief [brief description]
         * @details [long description]
         */
        LegendOptions &setBoxed(const bool &boxed);

        /**
         * @brief [brief description]
         * @details [long description]
         */
        const bool &getShow() const;

        /**
         * @brief [brief description]
         * @details [long description]
         */
        const Anchor &getAnchor() const;

        /**
         * @brief [brief description]
         * @details [long description]
         */
        const bool &getBoxed() const;

      private:
        ///
        bool show_;
        /// where to anchor the Legen with respect to the Plot
        Anchor anchor_;
        /// true: draw a black box around the Legen
        bool boxed_;
    };
}  // namespace scatter