/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <scatter/LegendOptions.hpp>

namespace scatter
{

    LegendOptions::LegendOptions()
    {
        show_ = true;
        anchor_ = Anchor::NORTH_EAST;
        boxed_ = true;
    }

    LegendOptions::~LegendOptions() {}

    LegendOptions &LegendOptions::setShow(const bool &show)
    {
        show_ = show;

        return *this;
    }

    LegendOptions &LegendOptions::setAnchor(const Anchor &anchor)
    {
        anchor_ = anchor;

        return *this;
    }

    LegendOptions &LegendOptions::setBoxed(const bool &boxed)
    {
        boxed_ = boxed;

        return *this;
    }

    const bool &LegendOptions::getShow() const
    {
        return show_;
    }

    const Anchor &LegendOptions::getAnchor() const
    {
        return anchor_;
    }

    const bool &LegendOptions::getBoxed() const
    {
        return boxed_;
    }
}  // namespace scatter