/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <string>
#include <utility>
//
#include <scatter/Colour.hpp>
#include <scatter/TextOptions.hpp>

namespace scatter
{
    class Point;

    /**
     * @brief [brief description]
     * @details [long description]
     * @return [description]
     */
    class Renderer
    {
      public:
        /**
         * @brief [brief description]
         * @details [long description]
         */
        Renderer();

        /**
         * @brief [brief description]
         * @details [long description]
         */
        virtual ~Renderer();

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param p1 [description]
         * @param p2 [description]
         */
        virtual void restrictArea(const Point &p1, const Point &p2) = 0;

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param points [description]
         * @param colour [description]
         */
        virtual void area(const std::vector<Point> &points, const Colour &colour = Colour()) = 0;

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param p [description]
         * @param radius [description]
         * @param colour [description]
         * @param fill [description]
         */
        virtual void circle(const Point &p, const double &radius, const Colour &colour = Colour(), bool fill = true) = 0;

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param p1 [description]
         * @param p2 [description]
         * @param colour [description]
         * @param line_width [description]
         */
        virtual void line(const Point &p1, const Point &p2, const Colour &colour = Colour(), const double &line_width = 0.1) = 0;

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param p1 [description]
         * @param p2 [description]
         * @param colour [description]
         * @param fill [description]
         */
        virtual void rectangle(const Point &p1, const Point &p2, const Colour &colour = Colour(), bool fill = true) = 0;

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param p [description]
         * @param minor [description]
         * @param major [description]
         * @param rotation [description]
         * @param colour [description]
         * @param fill [description]
         */
        virtual void ellipse(const Point &p, const double &minor, const double &major, const double &rotation, const Colour &colour = Colour(),
                             bool fill = true) = 0;

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param p [description]
         * @param radius [description]
         * @param colour [description]
         */
        virtual void asterisk(const Point &p, const double &radius, const Colour &colour = Colour()) = 0;

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param p [description]
         * @param radius [description]
         * @param colour [description]
         */
        virtual void cross(const Point &p, const double &radius, const Colour &colour = Colour()) = 0;

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param p [description]
         * @param radius [description]
         * @param colour [description]
         */
        virtual void plus(const Point &p, const double &radius, const Colour &colour = Colour()) = 0;

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param p [description]
         * @param radius [description]
         * @param colour [description]
         * @param fill [description]
         */
        virtual void triangle(const Point &p, const double &radius, const Colour &colour = Colour(), bool fill = true) = 0;

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param p [description]
         * @param radius [description]
         * @param colour [description]
         * @param fill [description]
         */
        virtual void diamond(const Point &p, const double &radius, const Colour &colour = Colour(), bool fill = true) = 0;

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param p [description]
         * @param radius [description]
         * @param colour [description]
         * @param fill [description]
         */
        virtual void square(const Point &p, const double &radius, const Colour &colour = Colour(), bool fill = true) = 0;

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param p [description]
         * @param radius [description]
         * @param colour [description]
         * @param fill [description]
         */
        virtual void star(const Point &p, const double &radius, const Colour &colour = Colour(), bool fill = true) = 0;

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param p1 [description]
         * @param p2 [description]
         * @param p3 [description]
         * @param p4 [description]
         * @param colour [description]
         */
        virtual void arrow(const Point &p1, const Point &p2, const Point &p3, const Point &p4, const Colour &colour = Colour()) = 0;

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param p [description]
         * @param label [description]
         * @param options [description]
         */
        virtual void text(const Point &p, const std::string &label, const TextOptions &options = TextOptions()) = 0;

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param p [description]
         * @param label [description]
         * @param anchor [description]
         */
        virtual void text(const Point &p, const std::string &label, const Anchor &anchor) = 0;

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param options [description]
         * @return [description]
         */
        virtual double getCharWidth(const TextOptions &options) = 0;

      protected:
      private:
    };

}  // namespace scatter