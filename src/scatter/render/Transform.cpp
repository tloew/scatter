/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <cmath>
#include <iomanip>
#include <iostream>
#include <scatter/Arrow.hpp>
#include <scatter/Ellipse.hpp>
#include <scatter/PlotOptions.hpp>
#include <scatter/render/Transform.hpp>
#include <sstream>

namespace scatter
{
    Transform::Transform(const Point &origin, const double &width, const double &height, const PlotOptions &options)
      : origin_(origin), options_(options)
    {
        p_x_min_ = options.getAxisOptions().getXmin();
        p_x_max_ = options.getAxisOptions().getXmax();
        p_y_min_ = options.getAxisOptions().getYmin();
        p_y_max_ = options.getAxisOptions().getYmax();

        width_ = width;
        height_ = height;

        double text_point = options.getTextOptions().getSize();

        if (options.getAxisOptions().getShow())
        {
            if (options.getAxisOptions().getYlabel().empty())
            {
                x_min_ = origin.x() + (0.75 * options.getAxisOptions().getXPrecision()) * text_point;
            }
            else
            {
                x_min_ = origin.x() + (1.5 * options.getAxisOptions().getXPrecision()) * text_point;
            }

            x_max_ = origin.x() + width - 0.5 * text_point;

            if (options.getTitle().empty())
            {
                y_min_ = origin.y() + 0.55 * text_point;
            }
            else
            {
                y_min_ = origin.y() + 2.5 * options.getTitleOptions().getSize();
            }

            if (!options.getAxisOptions().getXlabel().empty())
            {
                y_max_ = origin.y() + height - 3.75 * text_point;
            }
            else
            {
                if (options.getAxisOptions().getShowXticks())
                {
                    y_max_ = origin.y() + height - 1.75 * text_point;
                }
                else
                {
                    y_max_ = origin.y() + height - 0.55 * text_point;
                }
            }
        }
        else
        {
            x_min_ = origin.x() + text_point;
            x_max_ = origin.x() + width - text_point;

            y_min_ = origin.y() + text_point;
            y_max_ = origin.y() + height - text_point;
        }

        x_scale_ = (x_max_ - x_min_) / (options.getAxisOptions().getXmax() - options.getAxisOptions().getXmin());
        x_delta_ = options.getAxisOptions().getXmax() - options.getAxisOptions().getXmin();

        y_scale_ = (y_max_ - y_min_) / (options.getAxisOptions().getYmax() - options.getAxisOptions().getYmin());
        y_delta_ = options.getAxisOptions().getYmax() - options.getAxisOptions().getYmin();
    }

    Point Transform::operator()(const Point &point) const
    {
        return Point(x(point.x()), y(point.y()));
    }

    Ellipse Transform::operator()(const Ellipse &ellipse) const
    {
        return Ellipse((*this)(ellipse.getCenter()), y_scale_ * ellipse.getMinor(), x_scale_ * ellipse.getMajor(),
                       ellipse.getRotation() * y_scale_ / x_scale_);
    }

    Arrow Transform::operator()(const Arrow &arrow) const
    {
        return Arrow((*this)(arrow.getPoints()[0]), (*this)(arrow.getPoints()[1]));
    }

    Point Transform::operator()(const double &xval, const double &yval) const
    {
        return Point(x(xval), y(yval));
    }

    double Transform::x(const double &val) const
    {
        return x_min_ + (val - p_x_min_) * x_scale_;
    }

    double Transform::y(const double &val) const
    {
        return y_max_ - (val - p_y_min_) * y_scale_;
    }

    Point Transform::absolute(const double &xscale, const double &yscale) const
    {
        return Point(origin_.x() + xscale * width_, origin_.y() + height_ - yscale * height_);
    }

    Point Transform::relative(const double &xscale, const double &yscale) const
    {
        return Point(x(p_x_min_ + xscale * (p_x_max_ - p_x_min_)), y(p_y_min_ + yscale * (p_y_max_ - p_y_min_)));
    }

    const PlotOptions &Transform::getOptions() const
    {
        return options_;
    }

}  // namespace scatter