/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <scatter/PlotOptions.hpp>
#include <scatter/Point.hpp>

namespace scatter
{
    class Ellipse;
    class Arrow;

    /**
     * @brief [brief description]
     * @details [long description]
     *
     */
    class Transform
    {
      public:
        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param origin [description]
         * @param width [description]
         * @param height [description]
         * @param options [description]
         */
        // Transform(const Point &origin, const double &width, const double &height, const double &xmin, const double &xmax, const double &ymin, const
        // double &ymax);
        Transform(const Point &origin, const double &width, const double &height, const PlotOptions &options);

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param point [description]
         */
        Point operator()(const Point &point) const;

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param ellipse [description]
         */
        Ellipse operator()(const Ellipse &ellipse) const;

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param arrow [description]
         */
        Arrow operator()(const Arrow &arrow) const;

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param xval [description]
         * @param yval [description]
         */
        Point operator()(const double &xval, const double &yval) const;

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param xscale [description]
         * @param yscale [description]
         */
        Point absolute(const double &xscale, const double &yscale) const;

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param xscale [description]
         * @param yscale [description]
         */
        Point relative(const double &xscale, const double &yscale) const;

        /**
         * @brief [brief description]
         * @details [long description]
         * @return [description]
         */
        const PlotOptions &getOptions() const;

      private:
        ///
        const Point origin_;

        ///
        double width_;
        ///
        double height_;

        ///
        double x_min_;
        ///
        double x_max_;
        ///
        double x_scale_;
        ///
        double x_delta_;

        ///
        double y_min_;
        ///
        double y_max_;
        ///
        double y_scale_;
        ///
        double y_delta_;

        ///
        double p_x_min_;
        ///
        double p_x_max_;
        ///
        double p_y_min_;
        ///
        double p_y_max_;

        PlotOptions options_;

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param val [description]
         * @return [description]
         */
        double x(const double &val) const;

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param val [description]
         * @return [description]
         */
        double y(const double &val) const;
    };
}  // namespace scatter