/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <pangomm/init.h>
#include <pangomm/layout.h>

#include <iostream>
#include <scatter/Colour.hpp>
#include <scatter/Point.hpp>
#include <scatter/TextOptions.hpp>
#include <scatter/render/CairoRenderer.hpp>

namespace scatter
{
    CairoRenderer::CairoRenderer(const std::string &filename, const double &width, const double &height)
    {
        surface_ = Cairo::PdfSurface::create(filename, width, height);
        context_ = Cairo::Context::create(surface_);

        Pango::init();
    }

    CairoRenderer::~CairoRenderer() {}

    void CairoRenderer::area(const std::vector<Point> &points, const Colour &colour)
    {
        if (points.empty())
        {
            return;
        }

        draw(
          [this, &points]() {
              context_->move_to(points.front().x(), points.front().y());

              for (unsigned i = 1; i < points.size(); ++i)
              {
                  context_->line_to(points[i].x(), points[i].y());
              }
          },
          colour, false, 1.0);
    }

    void CairoRenderer::circle(const Point &p, const double &radius, const Colour &colour, bool fill)
    {
        draw([this, &p, &radius]() { context_->arc(p.x(), p.y(), radius, 0.0, 2.0 * M_PI); }, colour, fill);
    }

    void CairoRenderer::line(const Point &p1, const Point &p2, const Colour &colour, const double &line_width)
    {
        draw(
          [this, &p1, &p2]() {
              context_->move_to(p1.x(), p1.y());
              context_->line_to(p2.x(), p2.y());
          },
          colour, false, line_width);
    }

    void CairoRenderer::rectangle(const Point &p1, const Point &p2, const Colour &colour, bool fill)
    {
        draw(
          [this, &p1, &p2]() {
              context_->move_to(p1.x(), p1.y());
              context_->line_to(p1.x(), p2.y());
              context_->line_to(p2.x(), p2.y());
              context_->line_to(p2.x(), p1.y());
              context_->line_to(p1.x(), p1.y());
              context_->close_path();
          },
          colour, fill);
    }

    void CairoRenderer::ellipse(const Point &p, const double &minor, const double &major, const double &rotation, const Colour &colour, bool fill)
    {
        draw(
          [this, &p, &minor, &major, &rotation]() {
              Cairo::Matrix matrix;
              context_->get_matrix(matrix);
              context_->translate(p.x(), p.y());
              context_->rotate_degrees(-rotation);
              context_->scale(major, minor);
              context_->arc(0.0, 0.0, 1.0, 0.0, 2 * M_PI);
              context_->set_matrix(matrix);
          },
          colour, fill);
    }

    void CairoRenderer::asterisk(const Point &p, const double &radius, const Colour &colour)
    {
        plus(p, radius, colour);
        cross(p, radius, colour);
    }

    void CairoRenderer::cross(const Point &p, const double &radius, const Colour &colour)
    {
        draw(
          [this, &p, &radius]() {
              context_->move_to(p.x(), p.y());
              context_->rel_move_to(-radius * std::sin(M_PI / 4.0), -radius * std::cos(M_PI / 4.0));
              context_->rel_line_to(2.0 * radius * std::sin(M_PI / 4.0), 2.0 * radius * std::cos(M_PI / 4.0));

              context_->move_to(p.x(), p.y());
              context_->rel_move_to(-radius * std::sin(M_PI / 4.0), radius * std::cos(M_PI / 4.0));
              context_->rel_line_to(2.0 * radius * std::sin(M_PI / 4.0), -2.0 * radius * std::cos(M_PI / 4.0));
          },
          colour, false);
    }

    void CairoRenderer::plus(const Point &p, const double &radius, const Colour &colour)
    {
        draw(
          [this, &p, &radius]() {
              context_->move_to(p.x(), p.y());
              context_->rel_move_to(-radius, 0.0);
              context_->rel_line_to(2.0 * radius, 0.0);

              context_->move_to(p.x(), p.y());
              context_->rel_move_to(0.0, -radius);
              context_->rel_line_to(0.0, 2.0 * radius);
          },
          colour, false);
    }

    void CairoRenderer::triangle(const Point &p, const double &radius, const Colour &colour, bool fill)
    {
        draw(
          [this, &p, &radius]() {
              context_->move_to(p.x(), p.y() - radius);
              context_->line_to(p.x() + radius * std::sin(M_PI / 3.0), p.y() + radius * std::cos(M_PI / 3.0));
              context_->line_to(p.x() - radius * std::sin(M_PI / 3.0), p.y() + radius * std::cos(M_PI / 3.0));
              context_->line_to(p.x(), p.y() - radius);
          },
          colour, fill);
    }

    void CairoRenderer::diamond(const Point &p, const double &radius, const Colour &colour, bool fill)
    {
        draw(
          [this, &p, &radius]() {
              context_->move_to(p.x() - radius / 2.0, p.y());
              context_->line_to(p.x(), p.y() - radius);
              context_->line_to(p.x() + radius / 2.0, p.y());
              context_->line_to(p.x(), p.y() + radius);
              context_->line_to(p.x() - radius / 2.0, p.y());
          },
          colour, fill);
    }

    void CairoRenderer::square(const Point &p, const double &radius, const Colour &colour, bool fill)
    {
        draw(
          [this, &p, &radius]() {
              double half_length = radius * std::sin(M_PI / 4.0);

              context_->move_to(p.x() - half_length, p.y() + half_length);
              context_->line_to(p.x() + half_length, p.y() + half_length);
              context_->line_to(p.x() + half_length, p.y() - half_length);
              context_->line_to(p.x() - half_length, p.y() - half_length);
              context_->line_to(p.x() - half_length, p.y() + half_length);
          },
          colour, fill);
    }

    void CairoRenderer::star(const Point &p, const double &radius, const Colour &colour, bool fill)
    {
        draw(
          [this, &p, &radius]() {
              context_->move_to(p.x(), p.y() - radius);

              for (int i = 1; i < 11; ++i)
              {
                  double angle = M_PI * (0.5 - static_cast<double>(i) / 5.0);
                  double distance = i % 2 == 0 ? radius : radius / 3.0;
                  context_->line_to(p.x() + distance * std::cos(angle), p.y() - distance * std::sin(angle));
              }
          },
          colour, fill);
    }

    void CairoRenderer::arrow(const Point &p1, const Point &p2, const Point &p3, const Point &p4, const Colour &colour)
    {
        line(p1, p2, colour);
        line(p2, p3, colour);
        line(p2, p4, colour);
    }

    void CairoRenderer::text(const Point &p, const std::string &label, const Anchor &anchor)
    {
        TextOptions options;
        options.setAnchor(anchor);

        text(p, label, options);
    }

    void CairoRenderer::text(const Point &p, const std::string &label, const TextOptions &options)
    {
        draw(
          [this, &p, &label, &options]() {
              Glib::RefPtr<Pango::Layout> layout = Pango::Layout::create(context_);

              layout->set_font_description(makeFont(options));
              layout->set_text(label);

              int w, h;
              layout->get_pixel_size(w, h);

              double xcorr = static_cast<double>(w) / 2.0 - 0.2;
              double ycorr = static_cast<double>(h) / 2.0 - 0.2;

              context_->move_to(p.x(), p.y());
              context_->rotate_degrees(-options.getRotation());

              switch (options.getAnchor())
              {
              case Anchor::CENTER:
                  context_->rel_move_to(-xcorr, -ycorr);
                  break;
              case Anchor::NORTH:
                  context_->rel_move_to(-xcorr, 0);
                  break;
              case Anchor::NORTH_EAST:
                  context_->rel_move_to(-2.0 * xcorr, 0);
                  break;
              case Anchor::EAST:
                  context_->rel_move_to(-2.0 * xcorr, -ycorr);
                  break;
              case Anchor::SOUTH_EAST:
                  context_->rel_move_to(-2.0 * xcorr, -2.0 * ycorr);
                  break;
              case Anchor::SOUTH:
                  context_->rel_move_to(-xcorr, -2.0 * ycorr);
                  break;
              case Anchor::SOUTH_WEST:
                  context_->rel_move_to(0.0, -2.0 * ycorr);
                  break;
              case Anchor::WEST:
                  context_->rel_move_to(0.0, -ycorr);
                  break;
              case Anchor::NORTH_WEST:
                  break;
              }

              layout->show_in_cairo_context(context_);
          },
          options.getColour(), false);
    }

    double CairoRenderer::getCharWidth(const TextOptions &options)
    {
        return static_cast<double>(
                 Pango::Layout::create(context_)->get_context()->load_font(makeFont(options))->get_metrics().get_approximate_char_width()) /
               Pango::SCALE;
    }

    Pango::FontDescription CairoRenderer::makeFont(const TextOptions &options)
    {
        Pango::FontDescription font;
        font.set_family("Anonymous Pro");
        font.set_size(options.getSize() * Pango::SCALE);
        font.set_style(Pango::Style::STYLE_NORMAL);

        if (options.getBold())
        {
            font.set_weight(Pango::Weight::WEIGHT_BOLD);
        }
        else
        {
            font.set_weight(Pango::Weight::WEIGHT_NORMAL);
        }

        return font;
    }

    void CairoRenderer::draw(const std::function<void()> &drawing_function, const Colour &colour, bool fill, const double &line_width)
    {
        context_->save();

        context_->set_source_rgba(colour.r(), colour.g(), colour.b(), colour.a());
        context_->set_line_width(line_width);
        context_->set_dash(std::vector<double>(), 0.0);
        context_->set_line_cap(Cairo::LineCap::LINE_CAP_ROUND);

        drawing_function();

        if (fill)
        {
            context_->fill();
        }
        else
        {
            context_->stroke();
        }

        context_->restore();
    }

    void CairoRenderer::restrictArea(const Point &p1, const Point &p2)
    {
        context_->reset_clip();
        context_->rectangle(p1.x(), p1.y(), p2.x() - p1.x(), p2.y() - p1.y());
        context_->clip();
    }

}  // namespace scatter