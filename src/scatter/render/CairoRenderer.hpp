/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <cairomm/context.h>
#include <cairomm/surface.h>
#include <pangomm/fontdescription.h>

#include <scatter/render/Renderer.hpp>
#include <string>

namespace scatter
{
    class Point;

    /**
     * @brief [brief description]
     * @details [long description]
     */
    class CairoRenderer : public Renderer
    {
      public:
        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param filename [description]
         * @param width [description]
         * @param height [description]
         */
        CairoRenderer(const std::string &filename, const double &width, const double &height);

        /**
         * @brief [brief description]
         * @details [long description]
         */
        virtual ~CairoRenderer();

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param p1 [description]
         * @param p2 [description]
         */
        void restrictArea(const Point &p1, const Point &p2);

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param points [description]
         * @param colour [description]
         */
        void area(const std::vector<Point> &points, const Colour &colour = Colour());

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param p [description]
         * @param radius [description]
         * @param colour [description]
         * @param fill [description]
         */
        void circle(const Point &p, const double &radius, const Colour &colour = Colour(), bool fill = true);

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param p1 [description]
         * @param p2 [description]
         * @param colour [description]
         * @param line_width [description]
         */
        void line(const Point &p1, const Point &p2, const Colour &colour = Colour(), const double &line_width = 0.1);

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param p1 [description]
         * @param p2 [description]
         * @param colour [description]
         * @param fill [description]
         */
        void rectangle(const Point &p1, const Point &p2, const Colour &colour = Colour(), bool fill = true);

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param p [description]
         * @param minor [description]
         * @param major [description]
         * @param rotation [description]
         * @param colour [description]
         * @param fill [description]
         */
        void ellipse(const Point &p, const double &minor, const double &major, const double &rotation, const Colour &colour = Colour(),
                     bool fill = true);

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param p [description]
         * @param radius [description]
         * @param colour [description]
         */
        void asterisk(const Point &p, const double &radius, const Colour &colour = Colour());

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param p [description]
         * @param radius [description]
         * @param colour [description]
         */
        void cross(const Point &p, const double &radius, const Colour &colour = Colour());

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param p [description]
         * @param radius [description]
         * @param colour [description]
         */
        void plus(const Point &p, const double &radius, const Colour &colour = Colour());

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param p [description]
         * @param radius [description]
         * @param colour [description]
         * @param fill [description]
         */
        void triangle(const Point &p, const double &radius, const Colour &colour = Colour(), bool fill = true);

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param p [description]
         * @param radius [description]
         * @param colour [description]
         * @param fill [description]
         */
        void diamond(const Point &p, const double &radius, const Colour &colour = Colour(), bool fill = true);

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param p [description]
         * @param radius [description]
         * @param colour [description]
         * @param fill [description]
         */
        void square(const Point &p, const double &radius, const Colour &colour = Colour(), bool fill = true);

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param p [description]
         * @param radius [description]
         * @param colour [description]
         * @param fill [description]
         */
        void star(const Point &p, const double &radius, const Colour &colour = Colour(), bool fill = true);

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param p1 [description]
         * @param p2 [description]
         * @param p3 [description]
         * @param p4 [description]
         * @param colour [description]
         */
        void arrow(const Point &p1, const Point &p2, const Point &p3, const Point &p4, const Colour &colour = Colour());

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param p [description]
         * @param label [description]
         * @param options [description]
         */
        void text(const Point &p, const std::string &label, const TextOptions &options = TextOptions());

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param p [description]
         * @param label [description]
         * @param anchor [description]
         */
        void text(const Point &p, const std::string &label, const Anchor &anchor);

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param options [description]
         * @return [description]
         */
        double getCharWidth(const TextOptions &options);

      protected:
        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param options [description]
         * @return [description]
         */
        Pango::FontDescription makeFont(const TextOptions &options);

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param drawing_function [description]
         * @param colour [description]
         * @param fill [description]
         * @param line_width [description]
         */
        void draw(const std::function<void()> &drawing_function, const Colour &colour, bool fill, const double &line_width = 0.1);

      private:
        ///
        Cairo::RefPtr<Cairo::Surface> surface_;
        ///
        Cairo::RefPtr<Cairo::Context> context_;
    };

}  // namespace scatter