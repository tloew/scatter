/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <array>
#include <string>
#include <vector>

namespace scatter
{
    /**
     * @class Colour Colour.hpp <scatter/Colour.hpp>
     * @brief define a RGBA colour
     * @details
     */
    class Colour
    {
      public:
        /**
         * @brief construct a black Colour
         * @details
         */
        Colour();

        /**
         * @brief constructor
         * @details construct a colour with values 0 <= x <= 1
         *
         * @param r red, 0 <= r <= 1
         * @param g green , 0 <= g <= 1
         * @param b blue, 0 <= b <= 1
         * @param a alpha, 0 <= a <= 1
         */
        Colour(const double &r, const double &g, const double &b, const double &a);

        /**
         * @brief constructor
         * @details construct a colour with values 0 <= x <= 255
         *
         * @param r red, 0 <= r <= 255
         * @param g green , 0 <= g <= 255
         * @param b blue, 0 <= b <= 255
         * @param a alpha, 0 <= a <= 255
         */
        Colour(const int &r, const int &g, const int &b, const int &a);

        /**
         * @brief constructor
         * @details construct a colour from a hex string
         *
         * @param hex_string rgba
         */
        Colour(const int &hex);

        /**
         * @brief destructor
         * @details [long description]
         */
        virtual ~Colour();

        /**
         * @brief return the red part
         * @details [long description]
         */
        const double &r() const;

        /**
         * @brief return the green part
         * @details [long description]
         */
        const double &g() const;

        /**
         * @brief return the blue part
         * @details [long description]
         */
        const double &b() const;

        /**
         * @brief return the alpha part
         * @details [long description]
         */
        const double &a() const;

      public:
        bool operator==(const Colour &other) const;

      protected:
        /**
         * @brief internal constructor
         * @details convenice constructor to be able to iterate over the values for saturating them
         *
         * @param c array containing r,g,b,a values
         */
        Colour(const std::array<double, 4> &c);

      private:
        /// array containing r,g,b,a values
        std::array<double, 4> data_;

      public:
        /// define red colour
        static Colour red;
        /// define blue colour
        static Colour blue;
        /// define green colour
        static Colour green;
        /// define yellow colour
        static Colour yellow;
        /// define black colour
        static Colour black;
        /// define white colour
        static Colour white;
        /// define cyan colour
        static Colour cyan;
        /// define pink colour
        static Colour pink;

      public:
        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param weight [description]
         * @param colour1 [description]
         * @param colour2 [description]
         * @return [description]
         */
        static Colour mix(const double &weight, const Colour &colour1, const Colour &colour2);

        /**
         * @brief [brief description]
         * @details [long description]
         *
         * @param hex [description]
         * @return [description]
         */
        static Colour createFromHex(const int &hex);
    };

}  // namespace scatter