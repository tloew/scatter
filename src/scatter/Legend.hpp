/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <scatter/Colour.hpp>
#include <scatter/LegendOptions.hpp>
#include <scatter/Plot.hpp>
#include <string>
#include <variant>
#include <vector>

namespace scatter
{
    /// forward declarations
    class Renderer;
    class Transform;

    /**
     * @brief [brief description]
     * @details private class, not meant to be used by the user
     */
    class Legend
    {
      public:
        /**
         * @brief [brief description]
         * @details [long description]
         */
        enum class Symbol
        {
            LINE,
            BAR,
            ARROW,
            ELLIPSE
        };

        /**
         * @brief [brief description]
         * @details [long description]
         */
        struct Entry
        {
            /// label of the plot
            std::string label;

            /// colour used for plotting
            Colour colour;

            /// Marker or Symbol representing the data
            std::variant<Marker, Symbol> symbol;
        };

      public:
        /**
         * @brief constructor
         * @details this class is instantiated within Plot
         *
         * @param options pointer to LegendOptions (coming from Plot)
         * @param text_options pointer to TextOptions (coming from Plot)
         */
        Legend(const LegendOptions *const options, const TextOptions *const text_options);

        /**
         * @brief destructor
         * @details [long description]
         */
        virtual ~Legend();

        /**
         * @brief add an entry to the Legend
         * @details [long description]
         *
         * @param entry Entry (created by a class inheriting from PlotBase)
         */
        void add(const Entry &entry);

        /**
         * @brief render the Legend using the given Renderer
         * @details [long description]
         *
         * @param renderer Renderer (base class)
         * @param transform Transform to convert from data to canvas coordinates
         */
        void render(Renderer &renderer, const Transform &transform);

      protected:
        /// pointer to LegendOptions (coming from Plot)
        const LegendOptions *const options_;

        /// pointer to TextOptions (coming from Plot)
        const TextOptions *const text_options_;

      private:
        /// array of Entry
        std::vector<Entry> entries_;

        /// storing the maximum number of letters of all entries to correctly render the box around the legend
        unsigned max_chars_;
    };

}  // namespace scatter