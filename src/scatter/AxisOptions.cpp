/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <scatter/AxisOptions.hpp>

namespace scatter
{
    AxisOptions::AxisOptions()
    {
        show_ = true;
        show_grid_ = true;
        xmin_ = 0.0;
        xmax_ = 0.0;
        xticks_ = 5.0;
        xlabel_ = "xlabel";
        ymin_ = 0.0;
        ymax_ = 0.0;
        yticks_ = 5.0;
        ylabel_ = "ylabel";
        x_precision_ = 3;
        y_precision_ = 3;
        show_xticks_ = true;
        show_yticks_ = true;
        y_log_scale_ = false;
    }

    AxisOptions::~AxisOptions() {}

    AxisOptions &AxisOptions::setShow(const bool &show)
    {
        show_ = show;

        return *this;
    }

    AxisOptions &AxisOptions::setShowGrid(const bool &show_grid)
    {
        show_grid_ = show_grid;

        return *this;
    }

    AxisOptions &AxisOptions::setXmin(const double &xmin)
    {
        xmin_ = xmin;

        return *this;
    }

    AxisOptions &AxisOptions::setXmax(const double &xmax)
    {
        xmax_ = xmax;

        return *this;
    }

    AxisOptions &AxisOptions::setXticks(const double &xticks)
    {
        xticks_ = xticks;

        return *this;
    }

    AxisOptions &AxisOptions::setShowXticks(const bool &show_xticks)
    {
        show_xticks_ = show_xticks;

        return *this;
    }

    AxisOptions &AxisOptions::setXlabel(const std::string &xlabel)
    {
        xlabel_ = xlabel;

        return *this;
    }

    AxisOptions &AxisOptions::setYmin(const double &ymin)
    {
        ymin_ = ymin;

        return *this;
    }

    AxisOptions &AxisOptions::setYmax(const double &ymax)
    {
        ymax_ = ymax;

        return *this;
    }

    AxisOptions &AxisOptions::setYticks(const double &yticks)
    {
        yticks_ = yticks;

        return *this;
    }

    AxisOptions &AxisOptions::setShowYticks(const bool &show_yticks)
    {
        show_yticks_ = show_yticks;

        return *this;
    }

    AxisOptions &AxisOptions::setYlabel(const std::string &ylabel)
    {
        ylabel_ = ylabel;

        return *this;
    }

    AxisOptions &AxisOptions::setXPrecision(const int &precision)
    {
        x_precision_ = precision;

        return *this;
    }

    AxisOptions &AxisOptions::setYPrecision(const int &precision)
    {
        y_precision_ = precision;

        return *this;
    }

    AxisOptions &AxisOptions::setYLogScale(const bool &y_log_scale)
    {
        y_log_scale_ = y_log_scale;

        return *this;
    }

    const bool &AxisOptions::getShow() const
    {
        return show_;
    }

    const bool &AxisOptions::getShowGrid() const
    {
        return show_grid_;
    }

    const double &AxisOptions::getXmin() const
    {
        return xmin_;
    }

    const double &AxisOptions::getXmax() const
    {
        return xmax_;
    }

    const double &AxisOptions::getXticks() const
    {
        return xticks_;
    }

    const bool &AxisOptions::getShowXticks() const
    {
        return show_xticks_;
    }

    const std::string &AxisOptions::getXlabel() const
    {
        return xlabel_;
    }

    const double &AxisOptions::getYmin() const
    {
        return ymin_;
    }

    const double &AxisOptions::getYmax() const
    {
        return ymax_;
    }

    const double &AxisOptions::getYticks() const
    {
        return yticks_;
    }

    const bool &AxisOptions::getShowYticks() const
    {
        return show_yticks_;
    }

    const std::string &AxisOptions::getYlabel() const
    {
        return ylabel_;
    }

    const int &AxisOptions::getXPrecision() const
    {
        return x_precision_;
    }

    const int &AxisOptions::getYPrecision() const
    {
        return y_precision_;
    }

    const bool &AxisOptions::getYLogScale() const
    {
        return y_log_scale_;
    }

}  // namespace scatter