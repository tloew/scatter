/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <scatter/Colour.hpp>
#include <scatter/ColourScheme.hpp>

namespace scatter
{

    ColourScheme::ColourScheme()
      : ColourScheme("default", {
                                  Colour::red,
                                  Colour::blue,
                                  Colour::green,
                                  Colour::yellow,
                                  Colour::black,
                                  Colour::white,
                                  Colour::cyan,
                                  Colour::pink,
                                })
    {}

    ColourScheme::ColourScheme(const std::string &name, const std::vector<Colour> &colours) : name_(name), colours_(colours) {}

    const Colour &ColourScheme::getColour(const unsigned &index) const
    {
        return colours_[index % colours_.size()];
    }

    const std::string &ColourScheme::getName() const
    {
        return name_;
    }

}  // namespace scatter