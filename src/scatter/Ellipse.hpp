/*
 * Copyright (C) Tobias Löw (tobi.loew@protonmail.ch)
 *
 * This file is part of Scatter
 *
 * Scatter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scatter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scatter.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <scatter/Point.hpp>

namespace scatter
{
    /**
     * @brief [brief description]
     * @details [long description]
     */
    class Ellipse
    {
      public:
        /**
         * @brief constructor
         * @details [long description]
         *
         * @param center center Point
         * @param minor length of the minor axis
         * @param major length of the major axis
         * @param rotation angle between the major and the horizontal axis (in rad)
         */
        Ellipse(const Point &center, const double &minor, const double &major, const double &rotation);

        /**
         * @brief destructor
         * @details [long description]
         */
        virtual ~Ellipse();

        /**
         * @brief return the center Point
         * @details [long description]
         * @return center
         */
        const Point &getCenter() const;

        /**
         * @brief return the length of the minor axis
         * @details [long description]
         * @return length of the minor axis
         */
        const double &getMinor() const;

        /**
         * @brief return the length of the major axis
         * @details [long description]
         * @return length of the major axis
         */
        const double &getMajor() const;

        /**
         * @brief return the angle between the major and the horizontal axis (in rad)
         * @details [long description]
         * @return angle between the major and the horizontal axis (in rad)
         */
        const double &getRotation() const;

      protected:
      private:
        /// center Point of the Ellipse
        Point center_;
        /// length of the minor axis
        double minor_;
        /// length of the major axis
        double major_;
        /// angle between the major and the horizontal axis (in rad)
        double rotation_;
    };

}  // namespace scatter