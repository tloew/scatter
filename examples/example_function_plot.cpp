#include <cmath>
#include <scatter/scatter.hpp>

using namespace scatter;

int main(int /*argc*/, char ** /*argv*/)
{
    auto function = [](const double &x) {  //
        return 0.5 * std::sin(x) - 0.25 * std::cos(10.0 * x);
    };

    Plot::Ptr plot = Plot::create("0.5sin(x) - 0.25cos(10x)");

    plot->options()
      .getAxisOptions()
      .setXlabel("x")         //
      .setYlabel("y")         //
      .setXticks(M_PI / 3.0)  //
      .setYticks(0.25)        //
      .setXmin(-M_PI)         //
      .setXmax(M_PI)          //
      .setYmin(-1.0)          //
      .setYmax(1.0);

    auto *options = plot->add<FunctionPlot>(function, -M_PI, M_PI, 500);
    options->setLabel("function");
    options->setColour(Colour::red);

    plot->save("example_function_plot.pdf");
}