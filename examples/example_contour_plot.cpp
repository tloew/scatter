#include <cmath>
#include <scatter/scatter.hpp>

using namespace scatter;

int main(int /*argc*/, char ** /*argv*/)
{
    auto function = [](const double &x, const double &y) {  //
        return 1.001 + std::sin(2.0 * M_PI * Eigen::Vector2d(x - 1.0, y - 2.0).norm() / (0.5 * std::sqrt(2.0)));
    };

    Plot::Ptr plot = Plot::create();

    std::vector<Point> points = { Point(0.0, 0.0), Point(-3.0, -3.0), Point(-3.0, 3.0), Point(3.0, -3.0), Point(3.0, 3.0), Point(1.0, 2.0) };

    plot->add<ScatterPlot>(points);

    plot->add<ContourPlot>(function)->setNumLevels(1);

    plot->options()
      .getAxisOptions()
      .setXticks(1.0)  //
      .setYticks(1.0)  //
      .setXmin(-3.0)   //
      .setYmin(-3.0)   //
      .setXmax(3.0)    //
      .setYmax(3.0);

    plot->save("example_contour_plot.pdf");
}