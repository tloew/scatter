option(COMPILE_EXAMPLES "" OFF)

if(COMPILE_EXAMPLES)
	add_executable(example_contour_plot example_contour_plot.cpp)
	target_link_libraries(example_contour_plot scatter) # Eigen3::Eigen)
	target_include_directories(example_contour_plot
		PRIVATE
			$<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/..>
	)

	add_executable(example_function_plot example_function_plot.cpp)
	target_link_libraries(example_function_plot scatter) # Eigen3::Eigen)
	target_include_directories(example_function_plot
		PRIVATE
			$<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/..>
	)
endif()