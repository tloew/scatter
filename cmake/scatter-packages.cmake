find_package(PkgConfig REQUIRED)
pkg_search_module(CAIROMM --cflags --libs cairomm-1.0)	
pkg_search_module(PANGOMM --cflags --libs pangomm-1.4)

find_package(Eigen3 REQUIRED)