# scatter

Scatter is a c++ library that is targeted towards scientific plotting. It is currently still under development and can therefore still have breaking API changes. That being said, contributions are welcome! Please refer to [[CONTRIBUTING.md]] for further information.

## Dependencies

- cairomm
- pangomm
- Eigen (optional)

## Usage

## Examples

There are some examples available in [examples](examples). In order to compile them you need to add the following when running cmake 

    -DCOMPILE_EXAMPLES=ON

The available examples are 

- example_contour_plot.cpp
- example_function_plot.cpp

# License

Scatter is released under the GPLv3 license. Further information can be found in [[LICENSE.md]].